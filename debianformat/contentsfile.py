# Handle Contents files
# Copyright 2024 Thomas Karlsson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Thomas Karlsson thomas.karlsson relea.se

import os
import gzip
from typing import Dict, List, Any


class Contentinfo:
    """Handles a line in the Contents-amd64.gz file
    """
    def __init__(self, fact: str = '', factvalue: str = ''):
        """One line from Contents file

        Args:
            fact (str): path to the file
            factvalue (str): Which category and section the file belongs

        Returns:
            Contentinfo: the line as an object
        """
        self.factname = fact
        self.factvalue = factvalue

        self.continfo = self.__categorydict(fact, factvalue)

    def __categorydict(self, filename: str, secondstring: str) -> Dict[str, List[Dict[str, str]]]:
        """Private function to parse the line

        Args:
            filename (str): File path
            secondstring (str): Rest of the line excluding the file path

        Returns:
            Dict[str, List[Dict[str, str]]]
        """
        returndata: Dict[str, Any] = dict()
        returndata['filename'] = filename
        categories = list()
        for onecat in secondstring.split(','):
            catdata = dict()
            data = onecat.split('/')
            if len(data) == 2:
                catdata['category'] = data[0]
                catdata['package'] = data[1]
                catdata['section'] = ''
            if len(data) == 3:
                catdata['section'] = data[0]
                catdata['category'] = data[1]
                catdata['package'] = data[2]

            categories.append(catdata)

        returndata['categories'] = categories

        return returndata

    @property
    def name(self) -> str:
        """Filename

        Returns:
            str: The filename
        """
        return self.factname

    @property
    def sections(self) -> List[str]:
        """List unique sections for that file

        Returns:
            List[str]:
        """
        sec = list()
        for onesec in self.continfo['categories']:
            if onesec['section'] not in sec:
                sec.append(onesec['section'])

        return sec

    @property
    def categories(self) -> List[str]:
        """List unique categories for that file

        Returns:
            List[str]
        """
        sec = list()
        for onesec in self.continfo['categories']:
            if onesec['category'] not in sec:
                sec.append(onesec['category'])

        return sec

    @property
    def packages(self) -> List[str]:
        """List unique packages for that file

        Returns:
            List[str]
        """
        sec = list()
        for onesec in self.continfo['categories']:
            if onesec['package'] not in sec:
                sec.append(onesec['package'])

        return sec

    @property
    def info(self) -> List[Dict[str, str]]:
        return self.continfo['categories']

# {'filename': 'var/yp/nicknames', 'categories': [{'section': 'universe', 'category': 'net', 'package': 'nis'}, {'section': 'universe', 'category': 'net', 'package': 'yp-tools'}]}
    def add(self, category: str, package: str, section: str = '') -> Dict[str, str]:
        """Add new filename in Contentsfile

        Args:
            category (str): The category for the added file
            package (str): To which package does it belong
            section (str): To which section does it belong

        Returns:
            Dict[str, str] of added data
        """
        addinfo = False
        for one in self.continfo['categories']:
            if section != one['section'] and \
              category != one['category'] and \
              package != one['package']:
                addinfo = True
        if addinfo:
            self.continfo['categories'].append({'section': section,
                                                'category': category,
                                                'package': package})
            return {'section': section, 'category': category, 'package': package}

        return dict()

# {'filename': 'var/yp/nicknames', 'categories': [{'section': 'universe', 'category': 'net', 'package': 'nis'}, {'section': 'universe', 'category': 'net', 'package': 'yp-tools'}]}
    def remove(self, category: str, package: str, section: str = '') -> Dict[str, str]:
        """Remove a filename from Contents file

        Args:
            category (str): The category of the file
            package (str): To which package does it belong
            section (str): To which section does it belong

        Returns:
            Dict[str, str] of the removed file
        """
        removeinfo = False
        for one in self.continfo['categories']:
            if section != one['section'] and \
              category != one['category'] and \
              package != one['package']:
                removeinfo = True
        if removeinfo:
            self.continfo['categories'].remove({'section': section,
                                                'category': category,
                                                'package': package})
            return {'section': section, 'category': category, 'package': package}

        return dict()

    @property
    def totext(self) -> str:
        """Original text of the line

        Returns:
            str: The original string of the line in the Contents file
        """
        outstring = str()
        right = list()
        for one in self.info:
            thestring = str()
            if one['section'] != '':
                thestring += one['section'] + '/'
            thestring += one['category'] + '/' + one['package']
            right.append(thestring)
        outstring += self.factname
        factlen = len(self.factname.encode('utf-8'))
        if factlen <= 56:
            addtab = 1
            if (56 - factlen) % 8 == 0:
                addtab = 0
            padding = int((56 - factlen) / 8) + addtab
            for i in range(padding):
                outstring += "\t"

        if factlen > 56 and factlen < 60:
            for i in range(59 - factlen):
                outstring += ' '
        if factlen <= 56:
            outstring += '   '

        outstring += ' ' + ','.join(right)

        return outstring


class Contentsfile:
    """Handles the Contents-amd64.gz files"""
    def __init__(self):
        self.contentdata = dict()

    def load(self, contentsfile: str):
        """Loads the Contents file

        Args:
            contentsfile (str): Path to the file to load
        """
        self.rawdata = list()
        if not os.path.exists(contentsfile):
            raise FileNotFoundError(contentsfile)
        with gzip.open(contentsfile, mode='rt') as zipfile:
            self.rawdata = zipfile.readlines()

        self.contentdata = self.__parsecontentdata(self.rawdata)

    def raw(self) -> List[str]:
        """All the lines as they were read

        Returns:
            List[str]: Original data
        """
        return self.rawdata

    def fileinfo(self, filename: str) -> Contentinfo:
        """Gets a Contentinfo object of the file

        Args:
            filename (str): Which filename to get info about

        Returns:
            Contentinfo: File data about the requested file
        """
        if filename in self.contentdata:
            return self.contentdata[filename]

        return Contentinfo()

    def __parsecontentdata(self, releaselines: List[str]) -> Dict[str, Contentinfo]:
        """Private function for parsing Contents file

        Args:
            releaselines (List[str]): A list containing Contents file's lines

        Returns:
            Dict[str, Contentinfo]: All files in objects
        """
        content = dict()
        for line in self.rawdata:
            data = line.strip().rsplit(maxsplit=1)
            content[data[0].strip()] = Contentinfo(data[0].strip(),
                                                   data[1].strip())
        return content

    def alldata(self) -> Dict[str, Any]:
        """All data about the Contents file

        Returns:
            Dict[str, Any]: All files in objects
        """
        return self.contentdata

    @property
    def files(self) -> List[str]:
        """All file paths in Contents file

        Returns:
            List[str]: All file paths in the Contents file
        """
        return self.contentdata.keys()

    @property
    def totext(self) -> str:
        """Original text of the line

        Returns:
            str: The original Contents file
        """
        outstring = str()
        for onefile in self.files:
            outstring += self.fileinfo(onefile).totext + "\n"

        return outstring.strip()
