# Factinfo
# Copyright 2024 Thomas Karlsson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Thomas Karlsson thomas.karlsson relea.se

from typing import List, Dict, Union


class Factinfo:
    """Class for simple values in Debian

    E.g. Version: 3.2
    """
    def __init__(self, fact: str = '', factline: str = ''):
        """Init

        Args:
            fact (str): Name of the fact, example 'Version'
            factline (str): Value of the fact, example '3.2'
        """
        self.factvalue = factline
        self.factname = fact

    def __str__(self):
        return self.factvalue

    @property
    def name(self) -> str:
        """Fact name

        Returns:
            str: The name of the fact
        """
        return self.factname

    @name.setter
    def name(self, newname: str):
        """Set function of Fact name

        Args:
            newname (str): New name for fact name
        """
        self.factname = newname

    @property
    def value(self) -> str:
        """Vale of the fact

        Returns:
            str: The value of the fact name
        """
        return self.factvalue

    @value.setter
    def value(self, newvalue: str):
        """Set function of the fact value

        Args:
            newvalue (str): New value for fact
        """
        self.factvalue = newvalue

    @property
    def totext(self) -> str:
        """The original text of the value

        Returns:
            str: The original string line "3.2"
        """
        return self.factvalue

    @property
    def original(self) -> str:
        """The original text

        Returns:
            str: The original string line "Version: 3.2"
        """
        return self.name + ": " + self.totext


class Listinfo:
    """
    """
    def __init__(self, fact: str = '', tagdata: str = '', delimiter: str = ', '):
        self.factname = fact
        self.delimiter = delimiter
        self.tagdata = self.__process_tags(tagdata)

    def __process_tags(self, tagstring: str) -> List[str]:
        """ Returns a dict of dependencies
            keys are package, versioncompare and versionnumber
        """
        tags = list()
        for rawtag in tagstring.replace("\n", '').split(self.delimiter):
            if rawtag != '':
                tags.append(rawtag)

        return tags

    @property
    def name(self) -> str:
        return self.factname

    @name.setter
    def name(self, newname: str):
        self.factname = newname

    @property
    def value(self) -> List[str]:
        return self.tagdata

    def add(self, tagname: str):
        if not tagname:
            return
        self.tagdata.append(tagname)

    def remove(self, tagname: str):
        if not tagname:
            return
        self.tagdata.remove(tagname)

    @property
    def totext(self) -> str:
        return self.delimiter.join(self.tagdata).rstrip()

    @property
    def original(self) -> str:
        return self.name + ": " + self.totext


class Packagelistinfo:
    """
    """
    def __init__(self, fact: str = '', tagdata: str = '', delimiter: str = "\n"):
        self.factname = fact
        self.delimiter = delimiter
        self.tagdata = self.__process_tags(tagdata)

    def __process_tags(self, tagstring: str) -> List[str]:
        """ Returns a dict of dependencies
            keys are package, versioncompare and versionnumber
        """
        tags = list()
        for rawtag in tagstring.split(self.delimiter):
            if rawtag != '':
                tags.append(rawtag.strip())

        return tags

    def __multiline(self, fact: str, input: List[str], limit=1000) -> str:
        """Split into debian multiline

           Param:
            fact 'Binary', 'Description' etc.
            input ['firefox', 'firefox-locale-af', 'firefox-locale-an']
        """
        total_text = str()
        current_line = fact + ": "
        new_limit = limit - len(current_line)
        for element in input:
            if len(current_line) + len(element) + 2 < new_limit:
                current_line += element + ", "
            else:
                total_text += current_line.rstrip() + "\n"
                current_line = " "
                new_limit = limit - 1

        if current_line != ' ':
            total_text += current_line.rstrip(', ')

        return total_text

    @property
    def name(self) -> str:
        return self.factname

    @property
    def value(self) -> List[str]:
        return self.tagdata

    def add(self, tagname: str):
        if not tagname:
            return
        self.tagdata.append(tagname)

    def remove(self, tagname: str):
        if not tagname:
            return
        self.tagdata.remove(tagname)

    @property
    def totext(self) -> str:
        return self.delimiter.join(self.tagdata).rstrip()

    @property
    def original(self) -> str:
        return self.name + ": " + self.totext


class Dependsinfo:
    def __init__(self, depdata: str = '', mytype: str = 'normal'):
        """Dependency information

        Example:
            'passwd, debconf (>= 0.5) | debconf-2.0'

        Args:
            depdata: The "Depends: " string
            mytype: Can be set to "Python-Version"

        Returns:
            Dependsinfo object

        Raises:
            None
        """

        self.dependencydata = self.__process_depends(depdata)
        self.type = mytype

    def __process_depends(self, dependstring: str) -> List[List[Dict[str, str]]]:
        """Returns a dict of dependencies
           keys are package, versioncompare and versionnumber

        """
        deps: List[List[Dict[str, str]]] = list()
        if not dependstring:
            return deps
        for rawdependency in dependstring.split(', '):
            ordeps = list()
            for onepackage in rawdependency.split('|'):
                onedep = dict()
                dependencyversion = onepackage.strip().split(' ')
                temppackname = dependencyversion[0]
                if len(dependencyversion) == 1:
                    onedep['versioncompare'] = ''
                    onedep['versionnumber'] = ''
                if len(dependencyversion) == 2:
                    onedep['versioncompare'] = dependencyversion[0].strip()
                    onedep['versionnumber'] = dependencyversion[1].strip()
                    temppackname = ''
                if len(dependencyversion) == 3:
                    onedep['versioncompare'] = dependencyversion[1].lstrip('(')
                    onedep['versionnumber'] = dependencyversion[2].rstrip(')')
                onedep['package'] = temppackname
                ordeps.append(onedep)
            deps.append(ordeps)

        return deps

    @property
    def packages(self) -> List[str]:
        """Returns a list of package names
        It will return the first package if is in an or-statement
        'passwd, debconf (>= 0.5) | debconf-2.0'

        Args:
            None

        Returns:
            ['passwd', 'debconf']

        Raises:
            None
        """
        dependpackages = list()
        for onepackage in self.dependencydata:
            dependpackages.append(onepackage[0]['package'])
        return dependpackages

    @property
    def dependencies(self) -> List[List[Dict[str, str]]]:
        return self.dependencydata

    def __match(self, packagedata: Union[dict, List[Dict[str, str]]],
                findname: Union[str, List[str]],
                findnumber: Union[str, List[str]],
                findcompare: Union[str, List[str]]) -> bool:
        """
        Input:
            packagedata is a depinfo dictionary
            findname is a str/list with names to look for
            findnumber is a str/list with version numbers to look for
            findcompare is a str/list with compare operators to look for

        """
        sourcedata = packagedata
        searchname = findname
        searchnumber = findnumber
        searchcompare = findcompare
        if isinstance(packagedata, dict):
            sourcedata = [packagedata]
        if isinstance(findname, str):
            searchname = [findname]
        if isinstance(findnumber, str):
            searchnumber = [findnumber]
        if isinstance(findcompare, str):
            searchcompare = [findcompare]

        if len(sourcedata) != len(searchname):
            return False

        matchcounter = 0
        for deppackage in sourcedata:
            for searchindex, search in enumerate(searchname):
                if search == deppackage['package']:
                    matchcounter += 1
                    if searchnumber[searchindex]:
                        if searchnumber[searchindex] != deppackage['versionnumber']:
                            return False
                    if searchcompare[searchindex]:
                        if searchcompare[searchindex] != deppackage['versioncompare']:
                            return False

        if len(sourcedata) == matchcounter:
            return True

        return False

    def add(self, packagename: Union[str, List[str]],
            versionnumber: Union[str, List[str]] = '',
            versioncompare: Union[str, List[str]] = '') -> List[Dict[str, str]]:
        """Add a dependency"""
        orlist: List[Dict[str, str]] = list()

        if not packagename:
            return orlist

        if isinstance(packagename, str):
            vnumber = str()
            vcompare = str()
            if isinstance(versionnumber, list):
                vnumber = versionnumber[0]
            else:
                vnumber = versionnumber
            if isinstance(versioncompare, list):
                vcompare = versioncompare[0]
            else:
                vcompare = versioncompare
            depdata: Dict[str, str] = dict()
            depdata['versionnumber'] = vnumber
            depdata['versioncompare'] = vcompare
            depdata['package'] = packagename

            orlist.append(depdata)

        if isinstance(packagename, list):
            for index, onename in enumerate(packagename):
                depdata = dict()
                depdata['package'] = onename
                if isinstance(versionnumber, list):
                    depdata['versionnumber'] = versionnumber[index]
                else:
                    depdata['versionnumber'] = versionnumber
                if isinstance(versioncompare, list):
                    depdata['versioncompare'] = versioncompare[index]
                else:
                    depdata['versioncompare'] = versioncompare

                orlist.append(depdata)

        self.dependencydata.append(orlist)

        return orlist

    def remove(self, packagename: Union[str, List[str]],
               findversionnumber: Union[str, List[str]] = '',
               findversioncompare: Union[str, List[str]] = '') -> List[List[Dict[str, str]]]:
        """Remove a dependency"""
        newlist = list()
        for onedependency in self.dependencydata:
            if not self.__match(onedependency, packagename, findversionnumber,
                                findversioncompare):
                newlist.append(onedependency)

        self.dependencydata = newlist

        return self.dependencydata

    @property
    def totext(self) -> str:
        """Return to original text"""
        outlist = list()
        if self.type == 'Python-Version':
            start = ''
            end = ''
        else:
            start = ' ('
            end = ')'
        for requirement in self.dependencydata:
            outputstr = str()
            text = str()
            if len(requirement) == 1:
                outputstr += requirement[0]['package']
                if requirement[0]['versioncompare'] != '':
                    outputstr += start + requirement[0]['versioncompare'] + \
                                    " " + requirement[0]['versionnumber'] + end
                outlist.append(outputstr)
            if len(requirement) > 1:
                for reqoption in requirement:
                    text += reqoption['package']
                    if reqoption['versioncompare'] != '':
                        text += start + reqoption['versioncompare'] + \
                                   " " + reqoption['versionnumber'] + end
                    text += " | "
                outlist.append(text.rstrip(" |"))

        return ", ".join(outlist)
