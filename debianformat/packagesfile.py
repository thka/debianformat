# Handle Package files
# Copyright 2024 Thomas Karlsson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Thomas Karlsson thomas.karlsson relea.se

import os
import gzip
import lzma
from typing import Dict, List, Union, Any
import hashlib
from debianformat.debiantypes import Factinfo, Listinfo, Dependsinfo, Packagelistinfo


class Packageinfo:
    """
    Input is a list of dictionaries of packagedata

    """
    def __init__(self, packagedata: Dict[str, str] = {}, url_base: str = str(), directory_base: str = str()):
        self.packageinformation: Dict[str, Any] = dict()
        self.url_base = url_base
        self.directory_base = directory_base
        for fact in packagedata.keys():
            if fact in ['Depends', 'Recommends', 'Suggests', 'Conflicts',
                        'Replaces', 'Pre-Depends', 'Breaks', 'Build-Depends']:
                self.packageinformation[fact] = Dependsinfo(str(packagedata[fact]))
                continue
            elif fact in ['Python-Version']:
                self.packageinformation[fact] = Dependsinfo(str(packagedata[fact]), 'Python-Version')
                continue
            elif fact in ['Tag', 'Enhances', 'Task', 'Binary', 'Testsuite-Triggers']:
                self.packageinformation[fact] = Listinfo(fact, str(packagedata[fact]))
                continue
            elif fact in ['Closes']:
                self.packageinformation[fact] = Listinfo(fact, str(packagedata[fact]), delimiter=' ')
                continue
            elif fact in ['Package-List', 'Files', 'Checksums-Sha1', 'Checksums-Sha256']:
                self.packageinformation[fact] = Packagelistinfo(fact, str(packagedata[fact]))
                continue

            self.packageinformation[fact] = Factinfo(fact, str(packagedata[fact]))

    @property
    def language(self) -> str:
        languagedict = dict()
        for onefact in self.packageinformation.keys():
            splitstring = onefact.split('-')
            if any(map(str.isdigit, onefact)):
                continue
            if onefact in ['Description-md5', 'Description-SHA1']:
                continue
            if splitstring[0] == 'Description':
                if splitstring[1] not in languagedict:
                    languagedict[splitstring[1]] = 0
                languagedict[splitstring[1]] += 1

        return max(languagedict, key=lambda k: languagedict[k])

    @property
    def languages(self) -> List[str]:
        languagelist = list()
        for onefact in self.packageinformation.keys():
            splitstring = onefact.split('-')
            if any(map(str.isdigit, onefact)):
                continue
            if onefact in ['Description-md5', 'Description-SHA1']:
                continue
            if splitstring[0] == 'Description':
                if splitstring[1] not in languagelist:
                    languagelist.append(splitstring[1])

        return languagelist

    @property
    def url(self) -> str:
        if self.filename.value == str():
            return os.path.join(self.url_base.rstrip('/'), self.directory.value)

        return os.path.join(self.url_base.rstrip('/'), self.filename.value)

    @property
    def path(self) -> str:
        """
        If self.filename is empty, its probably a Sources.gz file
        """
        if self.filename.value == str():
            return os.path.join(self.directory_base.rstrip('/'), self.directory.value)

        return os.path.join(self.directory_base.rstrip('/'), self.filename.value)

    @property
    def totext(self) -> str:
        """Return to original text"""
        originaltext = str()
        for key in self.packageinformation:
            originaltext += key + ": " + self.packageinformation[key].totext + "\n"
        return originaltext.rstrip()

    @property
    def build_depends(self) -> Dependsinfo:
        if 'Build-Depends' in self.packageinformation:
            return self.packageinformation['Build-Depends']

        return Dependsinfo()

    @property
    def depends(self) -> Dependsinfo:
        if 'Depends' in self.packageinformation:
            return self.packageinformation['Depends']

        return Dependsinfo()

    @depends.setter
    def depends(self, dependsobject: Union[Dependsinfo, str]):
        if isinstance(dependsobject, str):
            self.packageinformation['Depends'] = Dependsinfo(dependsobject)
        if isinstance(dependsobject, Dependsinfo):
            self.packageinformation['Depends'] = dependsobject

    @property
    def recommends(self) -> Dependsinfo:
        if 'Recommends' in self.packageinformation:
            return self.packageinformation['Recommends']

        return Dependsinfo()

    @recommends.setter
    def recommends(self, recommendsobject: Union[Dependsinfo, str]):
        if isinstance(recommendsobject, str):
            self.packageinformation['Recommends'] = Dependsinfo(recommendsobject)
        if isinstance(recommendsobject, Dependsinfo):
            self.packageinformation['Recommends'] = recommendsobject

    @property
    def suggests(self) -> Dependsinfo:
        if 'Suggests' in self.packageinformation:
            return self.packageinformation['Suggests']

        return Dependsinfo()

    @suggests.setter
    def suggests(self, suggestsobject: Union[Dependsinfo, str]):
        if isinstance(suggestsobject, str):
            self.packageinformation['Suggests'] = Dependsinfo(suggestsobject)
        if isinstance(suggestsobject, Dependsinfo):
            self.packageinformation['Suggests'] = suggestsobject

    @property
    def conflicts(self) -> Dependsinfo:
        if 'Conflicts' in self.packageinformation:
            return self.packageinformation['Conflicts']

        return Dependsinfo()

    @conflicts.setter
    def conflicts(self, conflictsobject: Union[Dependsinfo, str]):
        if isinstance(conflictsobject, str):
            self.packageinformation['Conflicts'] = Dependsinfo(conflictsobject)
        if isinstance(conflictsobject, Dependsinfo):
            self.packageinformation['Conflicts'] = conflictsobject

    @property
    def replaces(self) -> Dependsinfo:
        if 'Replaces' in self.packageinformation:
            return self.packageinformation['Replaces']

        return Dependsinfo()

    @replaces.setter
    def replaces(self, replacesobject: Union[Dependsinfo, str]):
        if isinstance(replacesobject, str):
            self.packageinformation['Replaces'] = Dependsinfo(replacesobject)
        if isinstance(replacesobject, Dependsinfo):
            self.packageinformation['Replaces'] = replacesobject

    @property
    def enhances(self) -> Dependsinfo:
        if 'Enhances' in self.packageinformation:
            return self.packageinformation['Enhances']

        return Dependsinfo()

    @enhances.setter
    def enhances(self, replacesobject: Union[Dependsinfo, str]):
        if isinstance(replacesobject, str):
            self.packageinformation['Enhances'] = Dependsinfo(replacesobject)
        if isinstance(replacesobject, Dependsinfo):
            self.packageinformation['Enhances'] = replacesobject

    @property
    def pythonversion(self) -> Dependsinfo:
        if 'Python-Version' in self.packageinformation:
            return self.packageinformation['Python-Version']

        return Dependsinfo()

    @pythonversion.setter
    def pythonversion(self, pythonobject: Union[Dependsinfo, str]):
        if isinstance(pythonobject, str):
            self.packageinformation['Suggests'] = Dependsinfo(pythonobject)
        if isinstance(pythonobject, Dependsinfo):
            self.packageinformation['Suggests'] = pythonobject

    @property
    def package(self) -> Factinfo:
        """Returns the object fileinfo"""
        if 'Package' in self.packageinformation:
            return self.packageinformation['Package']

        return Factinfo()

    # @package.setter
    # def package(self, newname: str):
    #     """Sets package name"""
    #     self.packageinformation['Package'].value = newname

    @property
    def packagename(self) -> Factinfo:
        """Returns the package name"""
        return self.package

    # @packagename.setter
    # def packagename(self, newname: str):
    #     """Sets package name"""
    #     self.packageinformation['Package'].value = newname

    @property
    def tag(self) -> Listinfo:
        if 'Tag' in self.packageinformation:
            return self.packageinformation['Tag']

        return Listinfo()

    @property
    def closes(self) -> Listinfo:
        if 'Closes' in self.packageinformation:
            return self.packageinformation['Closes']

        return Listinfo()

    @property
    def urgency(self) -> Factinfo:
        """Returns a Factinfo object with the urgency"""
        if 'Urgency' in self.packageinformation:
            return self.packageinformation['Urgency']

        return Factinfo()

    @property
    def date(self) -> Factinfo:
        """Returns a Factinfo object with the Essential"""
        if 'Date' in self.packageinformation:
            return self.packageinformation['Date']

        return Factinfo()

    @property
    def essential(self) -> Factinfo:
        """Returns a Factinfo object with the Essential"""
        if 'Essential' in self.packageinformation:
            return self.packageinformation['Essential']

        return Factinfo()

    @property
    def changed_by(self) -> Factinfo:
        """Returns a Factinfo object with the Changed-By"""
        if 'Changed-By' in self.packageinformation:
            return self.packageinformation['Changed-By']

        return Factinfo()

    @property
    def package_list(self) -> Factinfo:
        """Returns a Factinfo object with the Package-List"""
        if 'Package-List' in self.packageinformation:
            return self.packageinformation['Package-List']

        return Factinfo()

    @property
    def standards_version(self) -> Factinfo:
        """Returns a Factinfo object with the standards_version"""
        if 'Standards-Version' in self.packageinformation:
            return self.packageinformation['Standards-Version']

        return Factinfo()

    @property
    def priority(self) -> Factinfo:
        """Returns a Factinfo object with the priority"""
        if 'Priority' in self.packageinformation:
            return self.packageinformation['Priority']

        return Factinfo()

    # @priority.setter
    # def priority(self, newprio: str):
    #     self.packageinformation['Priority'].value = newprio

    @property
    def section(self) -> Factinfo:
        """Returns a Factinfo object with the section"""
        if 'Section' in self.packageinformation:
            return self.packageinformation['Section']

        return Factinfo()

    # @section.setter
    # def section(self, newsection: str):
    #     self.packageinformation['Section'].value = newsection

    @property
    def installed_size(self) -> Factinfo:
        """Returns a Factinfo object with the installed size"""
        if 'Installed-Size' in self.packageinformation:
            return self.packageinformation['Installed-Size']

        return Factinfo()

    # @installedsize.setter
    # def installedsize(self, newsize: int):
    #     self.packageinformation['Installed-Size'].value = newsize

    @property
    def files(self) -> List[Dict[str, Union[int, str]]]:
        """Returns a Factinfo object with the files"""
        result: List[Dict[str, Union[int, str]]] = list()
        if 'Files' in self.packageinformation:
            for one in self.packageinformation['Files'].value:
                one_file: Dict[str, Union[int, str]] = dict()
                one_file['md5'] = one.strip().split(' ', maxsplit=2)[0]
                one_file['size'] = int(one.strip().split(' ', maxsplit=2)[1])
                one_file['filename'] = one.strip().split(' ', maxsplit=2)[2]
                for one in self.checksums_sha1.value:
                    if one.strip().split(' ', maxsplit=2)[2].strip() == one_file['filename']:
                        one_file['sha1'] = one.strip().split(' ', maxsplit=2)[0]
                for one in self.checksums_sha256.value:
                    if one.strip().split(' ', maxsplit=2)[2] == one_file['filename']:
                        one_file['sha256'] = one.strip().split(' ', maxsplit=2)[0]
                result.append(one_file)

            return result

        return list()

    @property
    def checksums_sha1(self) -> Packagelistinfo:
        """Returns a Packagelistinfo object with the files"""
        if 'Checksums-Sha1' in self.packageinformation:
            return self.packageinformation['Checksums-Sha1']

        return Packagelistinfo()

    @property
    def checksums_sha256(self) -> Packagelistinfo:
        """Returns a Packagelistinfo object with the files"""
        if 'Checksums-Sha256' in self.packageinformation:
            return self.packageinformation['Checksums-Sha256']

        return Packagelistinfo()

    @property
    def format(self) -> Factinfo:
        """Returns a Factinfo object with the format"""
        if 'Format' in self.packageinformation:
            return self.packageinformation['Format']

        return Factinfo()

    @property
    def binary(self) -> Listinfo:
        """Returns a Listinfo object with the binary"""
        if 'Binary' in self.packageinformation:
            return self.packageinformation['Binary']

        return Listinfo()

    @property
    def directory(self) -> Factinfo:
        """Returns a Factinfo object with the directory"""
        if 'Directory' in self.packageinformation:
            return self.packageinformation['Directory']

        return Factinfo()

    @property
    def maintainer(self) -> Factinfo:
        """Returns a Factinfo object with the maintainer"""
        if 'Maintainer' in self.packageinformation:
            return self.packageinformation['Maintainer']

        return Factinfo()

    # @maintainer.setter
    # def maintainer(self, newmaint: str):
    #     self.packageinformation['Maintainer'].value = newmaint

    @property
    def source(self) -> Factinfo:
        """Returns a Factinfo object with the source"""
        if 'Source' in self.packageinformation:
            return self.packageinformation['Source']

        return Factinfo()

    # @source.setter
    # def source(self, newsource: str):
    #     self.packageinformation['Source'].value = newsource

    @property
    def architecture(self) -> Factinfo:
        """Returns a Factinfo object with the achitecture"""
        if 'Architecture' in self.packageinformation:
            return self.packageinformation['Architecture']

        return Factinfo()

    # @architecture.setter
    # def architecture(self, newarch: str):
    #     self.packageinformation['Architecture'].value = newarch

    @property
    def version(self) -> Factinfo:
        """Returns a Factinfo object with the version"""
        if 'Version' in self.packageinformation:
            return self.packageinformation['Version']

        return Factinfo()

    # @version.setter
    # def version(self, newversion: str):
    #     self.packageinformation['Version'].value = newversion

    @property
    def filename(self) -> Factinfo:
        """Returns a Factinfo object with the filename"""
        if 'Filename' in self.packageinformation:
            return self.packageinformation['Filename']

        return Factinfo()

    # @filename.setter
    # def filename(self, newfilename: str):
    #     self.packageinformation['Filename'].value = newfilename

    @property
    def size(self) -> Factinfo:
        """Returns a Factinfo object with the size"""
        if 'Size' in self.packageinformation:
            return self.packageinformation['Size']

        return Factinfo()

    # @size.setter
    # def size(self, newsize: int):
    #     self.packageinformation['Size'].value = newsize

    @property
    def description(self) -> Factinfo:
        """Returns a Factinfo object with the description"""
        if 'Description' in self.packageinformation:
            return self.packageinformation['Description']

        return Factinfo()

    # @description.setter
    # def description(self, newdescription: str):
    #     self.packageinformation['Description'].value = newdescription

    def description_intl(self, language: str = str()) -> Factinfo:
        """Returns a Factinfo object with the description"""
        selected_language = language
        if language == str():
            selected_language = self.language
        if 'Description' + '-' + selected_language in self.packageinformation:
            return self.packageinformation['Description' + '-' + selected_language]

        return Factinfo()

    @property
    def description_md5(self) -> Factinfo:
        """Returns a Factinfo object with the description-md5"""
        if 'Description-md5' in self.packageinformation:
            return self.packageinformation['Description-md5']

        return Factinfo()

    @property
    def vcs_browser(self) -> Factinfo:
        """Returns a Factinfo object with the vcs_browser"""
        if 'Vcs-Browser' in self.packageinformation:
            return self.packageinformation['Vcs-Browser']

        return Factinfo()

    @property
    def vcs_git(self) -> Factinfo:
        """Returns a Factinfo object with the Vcs-Git"""
        if 'Vcs-Git' in self.packageinformation:
            return self.packageinformation['Vcs-Git']

        return Factinfo()

    @property
    def testsuite(self) -> Factinfo:
        """Returns a Factinfo object with the Testsuite"""
        if 'Testsuite' in self.packageinformation:
            return self.packageinformation['Testsuite']

        return Factinfo()

    @property
    def testsuite_triggers(self) -> Factinfo:
        """Returns a Factinfo object with the Testsuite-Triggers"""
        if 'Testsuite-Triggers' in self.packageinformation:
            return self.packageinformation['Testsuite-Triggers']

        return Factinfo()

    @property
    def uploaders(self) -> Factinfo:
        """Returns a Factinfo object with the homepage"""
        if 'Uploaders' in self.packageinformation:
            return self.packageinformation['Uploaders']

        return Factinfo()

    @property
    def homepage(self) -> Factinfo:
        """Returns a Factinfo object with the homepage"""
        if 'Homepage' in self.packageinformation:
            return self.packageinformation['Homepage']

        return Factinfo()

    # @homepage.setter
    # def homepage(self, newhomepage: str):
    #     self.packageinformation['Homepage'].value = newhomepage

    def unknown(self, unknownkey: str) -> Factinfo:
        if unknownkey in self.packageinformation:
            return self.packageinformation[unknownkey]

        return Factinfo()

    @property
    def md5(self) -> Factinfo:
        """Returns a Factinfo object with the md5"""
        if 'MD5sum' in self.packageinformation:
            return self.packageinformation['MD5sum']

        return Factinfo()

    # @md5.setter
    # def md5(self, newhash):
    #     if len(newhash) == 32:
    #         self.packageinformation['MD5sum'].value = newhash

    @property
    def sha1(self) -> Factinfo:
        """Returns a Factinfo object with the sha1"""
        if 'SHA1' in self.packageinformation:
            return self.packageinformation['SHA1']

        return Factinfo()

    # @sha1.setter
    # def sha1(self, newhash):
    #     if len(newhash) == 40:
    #         self.packageinformation['SHA1'].value = newhash

    @property
    def sha256(self) -> Factinfo:
        """Returns a Factinfo object with the sha256"""
        if 'SHA256' in self.packageinformation:
            return self.packageinformation['SHA256']

        return Factinfo()

    # @sha256.setter
    # def sha256(self, newhash: str):
    #     if len(newhash) == 64:
    #         self.packageinformation['SHA256'].value = newhash

    @property
    def sha512(self) -> Factinfo:
        """Returns a Factinfo object with the sha512"""
        if 'SHA512' in self.packageinformation:
            return self.packageinformation['SHA512']

        return Factinfo()

    # @sha512.setter
    # def sha512(self, newhash: str):
    #     if len(newhash) == 128:
    #         self.packageinformation['SHA512'].value = newhash

    @property
    def status(self) -> Factinfo:
        """Returns a Factinfo object with the status"""
        if 'Status' in self.packageinformation:
            return self.packageinformation['Status']

        return Factinfo()

    def custom(self, customproperty: str, customvalue=''):
        if customvalue != '':
            self.packageinformation[customproperty] = Factinfo(customproperty,
                                                               customvalue)
        if customproperty in self.packageinformation:
            return self.packageinformation[customproperty]


class Packagesfile:
    def __init__(self):
        self.rawpackagedata = []
        self.processedpackagedata: Dict[str, List[Packageinfo]] = {}
        self.globaldependency: List[str] = []
        self.version = '1.0'
        self.url_base = ''
        self.directory_base = ''
        self.packages_file = ''

    def load(self, packagefile: str) -> None:
        """Load package into memory"""
        if not os.path.exists(packagefile):
            raise FileNotFoundError(packagefile)
        if packagefile.endswith('gz'):
            with gzip.open(packagefile, mode='rt') as zipfile:
                self.rawpackagedata = zipfile.read()
        elif packagefile.endswith('xz') or packagefile.endswith('lzma'):
            with lzma.open(packagefile, mode='rt') as xzipfile:
                self.rawpackagedata = xzipfile.read()
        else:
            with open(packagefile, mode='rt') as textfile:
                self.rawpackagedata = textfile.read()

        self.packages_file = os.path.basename(packagefile)
        self.packagesdata = self.__parsepackagedata(self.rawpackagedata)
        # self.processedpackagedata = dict()
        for onepackage in self.packagesdata:
            if onepackage['Package'] not in self.processedpackagedata:
                self.processedpackagedata[onepackage['Package']] = [Packageinfo(
                    onepackage, self.url_base, self.directory_base)]
            else:
                self.processedpackagedata[onepackage['Package']].append(Packageinfo(
                    onepackage, self.url_base, self.directory_base))

        return None

    # @property
    # def url(self) -> str:
    #     return os.path.join(self.url_base.rstrip('/'), self.packages_file)

    # @property
    # def path(self) -> str:
    #     return os.path.join(self.directory_base.rstrip('/'), self.packages_file)

    @property
    def raw(self) -> str:
        """Returns original data"""
        outdata = str()
        for line in self.rawpackagedata:
            outdata += line

        outdata_stripped = outdata.rstrip()
        return outdata_stripped + "\n"

    def add(self, packagedata: Union[str, Dict[str, Any], Packageinfo]) -> Packageinfo:
        """
        Input is a dictionary of packagedata or a
        Packageinfo object

        """
        if isinstance(packagedata, str):
            # packetdatalist = packagedata.split("\n")
            # newlist = list(map(lambda x: x + "\n", packetdatalist))
            # newlist.append("\n")
            # pobject = self.__parsepackagedata(newlist)
            pobject = self.__parsepackagedata(packagedata)
            for oneobject in pobject:
                packageobject = Packageinfo(oneobject)
                if packageobject.packagename.value not in self.processedpackagedata:
                    self.processedpackagedata[packageobject.packagename.value] = [packageobject]
                else:
                    self.processedpackagedata[packageobject.packagename.value].append(packageobject)

            return packageobject
        if isinstance(packagedata, dict):
            packageobject = Packageinfo(packagedata)
            if packageobject.packagename.value not in self.processedpackagedata:
                self.processedpackagedata[packageobject.packagename.value] = [packageobject]
            else:
                self.processedpackagedata[packageobject.packagename.value].append(packageobject)

            return packageobject
        if isinstance(packagedata, Packageinfo):
            if packagedata.packagename.value not in self.processedpackagedata:
                self.processedpackagedata[packagedata.packagename.value] = [packagedata]
            else:
                self.processedpackagedata[packagedata.packagename.value].append(packagedata)

            return packagedata

        return Packageinfo()

    def remove(self, packagename: str, version: str = str()) -> bool:
        """Returns True if the package exists"""
        if packagename in self.processedpackagedata:
            if version == str():
                del self.processedpackagedata[packagename]
                return True
            else:
                ret_value = False
                temp_packages = list()
                for one in self.processedpackagedata[packagename]:
                    if one.version.value != version:
                        temp_packages.append(one)
                    else:
                        ret_value = True

                self.processedpackagedata[packagename] = temp_packages
                return ret_value

        return False

    def exists(self, packagename: str, version: str = str()) -> bool:
        """Returns true if package exists, else false"""
        if packagename in self.processedpackagedata:
            if version == str():
                return True

            for one_package in self.processedpackagedata[packagename]:
                if one_package.version.value == version:
                    return True

        return False

    def package(self, packagename: str, version: str = str()) -> List[Packageinfo]:
        """Returns the package object"""
        if packagename in self.processedpackagedata:
            if version == str():
                return self.processedpackagedata[packagename]
            for one_package in self.processedpackagedata[packagename]:
                if one_package.version.value == version:
                    return [one_package]

        return list()

    def importpackage(self, packagename: Packageinfo) -> List[Packageinfo]:
        """Import a class fileinfo package"""

        if packagename.package not in self.processedpackagedata:
            self.processedpackagedata[packagename.package.value] = [packagename]
        else:
            self.processedpackagedata[packagename.package.value].append(packagename)

        return self.processedpackagedata[packagename.package.value]

    @property
    def packages(self) -> List[str]:
        """Returns a list with all packagenames"""
        return list(self.processedpackagedata.keys())

    def __parsepackagedata(self, indata: str) -> List[Dict[str, str]]:
        """Returns a list with dictionaries of package data"""
        allpackages: List[Dict[str, str]] = list()
        packageinf = dict()
        lastline = str()
        data = list()
        fact = str()
        foundmultiline = False
        for raw_package in indata.split("\n\n"):
            for line in raw_package.split("\n"):
                # if not re.search(r'^ |^\t|:', line):
                if ':' not in line and not line.startswith(' ') and not line.startswith("\t"):
                    continue
                if line.strip() == '':
                    continue
                elif line.startswith(' '):
                    lastline += "\n" + line
                    foundmultiline = True
                else:
                    if foundmultiline:
                        packageinf[fact] = lastline.rstrip()
                        foundmultiline = False
                    data = line.split(':', maxsplit=1)
                    lastline = data[1].lstrip(' ')
                    fact = data[0]
                    # packageinf[fact] = lastline.rstrip("\n")
                    packageinf[fact] = lastline
            if foundmultiline:
                packageinf[fact] = lastline.rstrip()
            foundmultiline = False
            if packageinf != dict():
                # print(packageinf)
                allpackages.append(packageinf)
            packageinf = dict()

        return allpackages

    def packagedependency(self, packagename: str, version: str = str()) -> List[List[Dict[str, str]]]:
        """
        Returns the package dependencies, non recursively

        If its several versions, then show the first
        In the future, perhaps merge the list.
        """
        if version != str():
            return self.package(packagename, version=version)[0].depends.dependencies

        return self.package(packagename)[0].depends.dependencies
        # for one in self.package(packagename):
        #     print(one.depends.dependencies)

        # return self.package[packagename].depends.dependencies
        # # return self.processedpackagedata[packagename].depends.dependencies

    def __fulldependencylist(self, packagename: str) -> List[str]:
        if packagename not in self.processedpackagedata:
            return list()
        dependencylist = list()
        dependencylist.append(packagename)
        for requiredpackagelist in self.packagedependency(packagename):
            for requiredpackage in requiredpackagelist:
                if requiredpackage['package'] in self.globaldependency:
                    continue
                self.globaldependency.append(requiredpackage['package'])
                dependencylist.append(requiredpackage['package'])
                dependencylist += self.__fulldependencylist(
                    requiredpackage['package'])

        return [i for n, i in enumerate(dependencylist) if i not in dependencylist[:n]]

    def fulldependencylist(self, packagename: str, version: str = str()) -> List[str]:
        """Returns a list with all dependent packages, recursively"""
        dependencylist = self.__fulldependencylist(packagename)
        dependencylist.remove(packagename)
        dependencylist.sort()

        return dependencylist

    @property
    def totext(self) -> str:
        """Returns all packages as in Packages"""
        outstring = str()
        for onepackage in self.processedpackagedata.keys():
            for package_version in self.package(onepackage):
                outstring += package_version.totext
                outstring += "\n"
                outstring += "\n"

        returnstring = outstring.rstrip("\n")
        returnstring += "\n"

        return returnstring

    @property
    def md5(self) -> str:
        """Returns the md5 hash as a string"""
        packagehash = hashlib.md5()
        packagehash.update(self.totext.encode() + "\n".encode())

        return packagehash.hexdigest()

    @property
    def sha1(self) -> str:
        """Returns the sha1 hash as a string"""
        packagehash = hashlib.sha1()
        packagehash.update(self.totext.encode() + "\n".encode())

        return packagehash.hexdigest()

    @property
    def sha256(self) -> str:
        """Returns the sha256 hash as a string"""
        packagehash = hashlib.sha256()
        packagehash.update(self.totext.encode() + "\n".encode())

        return packagehash.hexdigest()
