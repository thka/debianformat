# Handle (In)Release files
# Copyright 2024 Thomas Karlsson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Thomas Karlsson thomas.karlsson relea.se

import os
import re
try:
    import pgpy  # type: ignore
    pgp_enabled = True
except ImportError:
    pgp_enabled = False

from typing import Dict, List, Any, Union
from debianformat.debiantypes import Factinfo, Listinfo


class Releasefileinfo:
    def __init__(self, releasefilename: str = '', filesize: int = None,
                 digestsum: str = '', complist: List[str] = [],
                 archlist: List[str] = [], url_base: str = str(),
                 directory_base: str = str()):
        self.url_base = url_base
        self.directory_base = directory_base
        self.releasefilename = releasefilename
        self.filesize = filesize
        self.md5: str = str()
        self.sha1: str = str()
        self.sha256: str = str()
        self.sha384: str = str()
        self.sha512: str = str()
        self.unknowndigest: str = str()

        self.digest(digestsum)

    @property
    def url(self) -> str:
        """
        File from Release is "main/binary-amd64/Packages.gz"

        Returns:
            The url to the file, if url_base was set
        Example:
            http://se.archive.ubumtu.com/ubuntu/dists/main/binary-amd64/Packages.gz
        """
        return os.path.join(self.url_base.rstrip('/'), self.relativepath)

    @property
    def path(self) -> str:
        """
        File from Release is "main/binary-amd64/Packages.gz"

        Returns:
            The path to the file, if directory_base was set
        Example:
            /data/repos/ubuntu/dists/main/binary-amd64/Packages.gz
        """
        return os.path.join(self.directory_base.rstrip('/'), self.relativepath)

    @property
    def component(self) -> str:
        """Component name
        The column is e.g. main/binary-amd64/Packages or Contents-amd64.gz
        If a slash exists, it returns main.
        If no slash exists, its not a path but just a filename,

        Args:
            None

        Returns:
            Component name e.g. main

        Raises:
            None
        """
        comp = self.relativepath.split('/')[0]
        if comp != self.relativepath:
            return comp

        return str()

    @property
    def function(self) -> str:
        """Function name
        The column is e.g. main/binary-amd64/Packages, Contents-amd64.gz
        main/debian-installer/binary-arm64/Packages.gz

        If a slash exists, it returns binary-amd64 or debian-installer.
        If no slash exists, its not a path but just a filename,

        Args:
            None

        Returns:
            Function name, e.g. binary-amd64, debian-installer

        Raises:
            None
        """
        func = self.relativepath.split('/')

        if len(func) > 1:
            # if 'binary-' in func[1]:
            #     return 'mandatory'
            return func[1]

        return self.relativepath

    @property
    def architecture(self) -> str:
        """Architecture of the line
        The column is e.g. main/binary-amd64/Packages, Contents-amd64.gz
        main/debian-installer/binary-arm64/Packages.gz

        Args:
            None

        Returns:
            Architecture or 'all'

        Raises:
            None
        """
        splitted = self.relativepath.split('/')
        for one in splitted:
            if 'binary-' in one:
                pre_arch = one.split('-')
                if re.search(r'[a-z0-9]+(\..*)?', pre_arch[1]):
                    return pre_arch[1].split('.')[0]
            if 'Commands-' in one:
                pre_arch = one.split('-')
                if re.search(r'[a-z0-9]+(\..*)?', pre_arch[1]):
                    return pre_arch[1].split('.')[0]
            if 'Contents-' in one:
                pre_arch = one.split('-')
                if re.search(r'[a-z0-9]+(\..*)?', pre_arch[1]):
                    return pre_arch[1].split('.')[0]
            if 'Components-' in one:
                pre_arch = one.split('-')
                if re.search(r'[a-z0-9]+(\..*)?', pre_arch[1]):
                    return pre_arch[1].split('.')[0]
            if 'installer-' in one:
                pre_arch = one.split('-')
                if re.search(r'[a-z0-9]+(\..*)?', pre_arch[1]):
                    return pre_arch[1].split('.')[0]

        return 'all'

    @property
    def language(self) -> str:
        """Current language

            restricted/i18n/Translation-cs.gz

        Args:
            None

        Returns:
            Current language or 'uknown' if it can't be determined
        """
        splitted = self.relativepath.split('/')
        if len(splitted) == 1:
            return 'unknown'
        for one in splitted:
            if 'Translation-' in one:
                return one.split('-')[1].split('.')[0]

        return 'unknown'

    @property
    def filename(self) -> str:
        """Filename of the file

        main/binary-amd64/Packages.bz2

        Args:
            None

        Returns:
            Basename of the file i.e. Packages.bz2
        """

        return os.path.basename(self.releasefilename)

    @property
    def filename_byhash(self) -> str:
        """Filename of the file

        The filename is the hash of the file

        Args:
            None

        Returns:
            Basename of the file i.e. the hash of the file
        """

        if self.sha256 != str():
            return self.sha256

        return str()

    @property
    def relativepath(self) -> str:
        """Relative path

        main/binary-amd64/Packages.bz2

        Args:
            None

        Returns:
            The path (main/binary-amd64/Packages.bz2)
        """

        return self.releasefilename

    @property
    def relativepath_byhash(self) -> str:
        """Relative path

        main/binary-amd64/by-hash/SHA256/1591942fef6b2d8c3d749ec666981e9a540ec0f1b2e38d4a189cd6dd77ef3b01

        Args:
            None

        Returns:
            The path (main/binary-amd64/by-hash/SHA256/1591942fef6b2d8c3d749ec666981e9a540ec0f1b2e38d4a189cd6dd77ef3b01)
        """

        return os.path.join(self.relativedirectory, 'by-hash', 'SHA256', self.filename_byhash)

    @property
    def relativedirectory(self) -> str:
        """Relative directory

        main/binary-amd64/Packages.bz2

        Args:
            None

        Returns:
            The path (main/binary-amd64)
        """

        return os.path.dirname(self.releasefilename)

    def digest(self, digestsum: str) -> None:
        """Sets the digest to correct value

        Args:
            Digest string

        Affects:
            self.md5, self.sha1, self.sha256, self.sha386, self, sha512

        Returns:
            None
        """

        if len(digestsum) == 32:
            self.md5 = digestsum
        elif len(digestsum) == 40:
            self.sha1 = digestsum
        elif len(digestsum) == 64:
            self.sha256 = digestsum
        elif len(digestsum) == 96:
            self.sha384 = digestsum
        elif len(digestsum) == 128:
            self.sha512 = digestsum
        else:
            self.unknowndigest = digestsum

    def totext(self, digest: str) -> str:
        """Normal original text

        Args:
            digest: Digest type ['md5', 'sha1', 'sha256', 'sha384', 'sha512']

        Returns:
            Original text line, as from file
        """

        if digest == 'md5':
            return " {0}{1:>17} {2}\n".format(self.md5, self.filesize,
                                              self.releasefilename)
        if digest == 'sha1':
            return " {0}{1:>17} {2}\n".format(self.sha1, self.filesize,
                                              self.releasefilename)
        if digest == 'sha256':
            return " {0}{1:>17} {2}\n".format(self.sha256, self.filesize,
                                              self.releasefilename)
        if digest == 'sha384':
            return " {0}{1:>17} {2}\n".format(self.sha384, self.filesize,
                                              self.releasefilename)
        if digest == 'sha512':
            return " {0}{1:>17} {2}\n".format(self.sha512, self.filesize,
                                              self.releasefilename)

        return " {0}{1:>17} {2}\n".format(self.unknowndigest, self.filesize,
                                          self.releasefilename)


class Releasefile:
    """ Read and parse a  Release file """
    def __init__(self):
        self.raw = ''
        self.rawdata = list()
        self.url_base = str()
        self.directory_base = str()
        self.release_file = str()
        self.md5 = False
        self.sha1 = False
        self.sha256 = False
        self.sha384 = False
        self.sha512 = False
        self.releasedata = {'headerdata': dict(),
                            'signdata': dict(),
                            'filedata': dict()}
        self.signers_public_key_file: str = ''
        self.signature_data = str()

    def load(self, releasefile: str):
        """Loads a release file

        Args:
            releasefile: Path to a Release file

        Returns:
            None
        """
        self.rawdata = list()
        if not os.path.exists(releasefile):
            raise FileNotFoundError(releasefile)
        self.release_file = os.path.basename(releasefile)
        with open(releasefile, mode='rb') as relfile:
            self.rawdata = relfile.read()

        self.releasedata = self.__parsereleasedata(self.rawdata.decode())

    def raw(self) -> str:
        """The unedited raw data

        Args:
            None

        Returns:
            Unedited raw data from the file

        Raises:
            None
        """

        return self.rawdata

    def __parsereleasedata(self, releaselines: str) -> Dict[str, Any]:
        """Private function for parsing data

        Args:
            releaselines (str): The whole file in a string

        Returns:
            Dict[str, Any]
        """
        headerdata: Dict[Any, Any] = dict()
        signdata: Dict[str, Any] = dict()
        filedata: Dict[Any, Any] = dict()
        releasedata = dict()
        foundheaderdata = False
        foundsignature = False
        signature = str()
        for line in releaselines.split('\n'):
            if line.strip().startswith('Hash:') and not foundheaderdata:
                signdata['digest'] = line.strip().split()[1]
                signdata['Hash'] = Factinfo('Hash',
                                            line.split(':', maxsplit=1)[1].strip())
            elif line.strip().startswith('Version:') and not foundheaderdata:
                signdata['Version'] = Factinfo('Version',
                                               line.split(':', maxsplit=1)[1].strip())
            elif line.strip() == '' and not foundheaderdata:
                foundheaderdata = True
            elif line.strip() == '-----BEGIN PGP SIGNATURE-----' and foundheaderdata:
                foundheaderdata = False
                foundsignature = True
                signature += line
            elif foundsignature:
                signature += line
            elif ':' in line.strip():
                foundheaderdata = True
                splittedline = line.split(':', maxsplit=1)
                currentkey = line.split(':', maxsplit=1)[0].strip()
                if currentkey in ['Architectures', 'Components']:
                    headerdata[currentkey] = Listinfo(currentkey,
                                                      splittedline[1].strip(),
                                                      ' ')
                else:
                    if splittedline[1].strip():
                        headerdata[line.split(':', maxsplit=1)[0].strip()] = \
                            Factinfo(splittedline[0].strip(),
                                     splittedline[1].strip())
            elif line.startswith(' '):
                linedata = line.strip().split()
                if linedata[2] in filedata:
                    filedata[linedata[2]].digest(linedata[0])
                else:
                    filedata[linedata[2]] = Releasefileinfo(linedata[2],
                                                            int(linedata[1]),
                                                            linedata[0],
                                                            url_base=self.url_base,
                                                            directory_base=self.directory_base)
                if len(linedata[0]) == 32:
                    self.md5 = True
                elif len(linedata[0]) == 40:
                    self.sha1 = True
                elif len(linedata[0]) == 64:
                    self.sha256 = True
                elif len(linedata[0]) == 96:
                    self.sha384 = True
                elif len(linedata[0]) == 128:
                    self.sha512 = True

        signdata['signature'] = Factinfo('Signature', signature.strip())

        releasedata['headerdata'] = headerdata
        releasedata['filedata'] = filedata
        releasedata['signdata'] = signdata

        return releasedata

    @property
    def url(self) -> str:
        """
        File from Release is "main/binary-amd64/Packages.gz"

        Returns:
            The url to the Release file, if url_base was set
        Example:
            http://se.archive.ubumtu.com/ubuntu/dists/main/Release
        """
        return os.path.join(self.url_base.rstrip('/'), 'dists', self.suite.value, self.release_file)

    @property
    def path(self) -> str:
        """
        Returns:
            The path to the file, if directory_base was set
        Example:
            /data/repos/ubuntu/dists/main/Release
        """
        return os.path.join(self.directory_base.rstrip('/'), 'dists', self.suite.value, self.release_file)

    @property
    def files(self) -> List[str]:
        """Files in the Releasefile
        ['main/binary-amd64/Packages.bz2']

        Args:
            None

        Returns:
            List with all files in the Release file
        """
        return self.releasedata['filedata'].keys()

    def fileinfo(self, filename: str) -> Releasefileinfo:
        """File information
        main/binary-amd64/Packages.bz2

        Args:
            filename: Filename to look for

        Returns:
            Releasefileinfo object
        """
        if filename in self.releasedata['filedata']:
            return self.releasedata['filedata'][filename]

        return Releasefileinfo()

    def file(self, filename: str) -> Releasefileinfo:
        """File information
        main/binary-amd64/Packages.bz2

        Args:
            filename: Filename to look for

        Returns:
            Releasefileinfo object
        """
        return self.fileinfo(filename)

    def add(self, filedata) -> None:
        """Add new Releasefileinfo object

        Args:
            filedata: Releasefileinfo object

        Returns:
            None

        Affects:
            self.releasedata
        """
        if isinstance(filedata, Releasefileinfo):
            self.releasedata['filedata'][filedata.releasefilename] = filedata

        return None

    def remove(self, filename: str) -> None:
        """Removes a file object

        Args:
            filename: String with the filename to remove

        Returns:
            None

        Affects:
            self.releasedata
        """
        if filename in self.releasedata['filedata']:
            del self.releasedata['filedata'][filename]

        return None

    def exists(self, filename: str) -> bool:
        """If a file exists

        Args:
            filename: String with the filename to remove

        Returns:
            If the file exists in Release file

        Affects:
            None
        """
        if filename not in self.releasedata['filedata']:
            return False

        return True

    def packages_files(self, component: str = str(),
                       architecture: str = str(),
                       diff: bool = False) -> List[Releasefileinfo]:
        """List Packages(.gz|xz|etc.)

        Args:
            component is main, restricted, etc.
            architecture is amd64, i386 etc.
            diff (boolean) return diff index files instead

        Returns:
            A list with file objects
        """
        retlist: List[Releasefileinfo] = list()
        search_string = str()
        repoformat = self.repoformat()
        if component != str():
            search_string = component + '/'
        if architecture != str():
            search_string += 'binary-' + architecture + '/'
        else:
            if repoformat == 'deb':
                search_string += 'binary-.+/'
            else:
                search_string += ''
        for onefile in self.files:
            if not diff:
                if '.diff' in onefile:
                    continue
                if re.search(search_string + 'Packages', onefile):
                    retlist.append(self.fileinfo(onefile))
            if diff:
                if re.search(search_string + 'Packages.diff/Index', onefile):
                    retlist.append(self.fileinfo(onefile))

        return retlist

    def source_files(self, component: str = str(),
                     diff: bool = False) -> List[Releasefileinfo]:
        """Returns all source files in the Release file

        Args:
            component (str): main, non-free, universe etc.
            diff (boolean) return diff index files instead

        Returns:
            Returns a list of Releasefileinfo objects with source files
        """
        retlist: List[Releasefileinfo] = list()
        for onefile in self.files:
            if not diff:
                if '.diff' in onefile:
                    continue
                if re.search(component + '/source/Sources', onefile):
                    retlist.append(self.fileinfo(onefile))
            if diff:
                if re.search(component + '/source/Sources.diff/Index', onefile):
                    retlist.append(self.fileinfo(onefile))

        return retlist

    def signatureinfo(self) -> Dict[str, Factinfo]:
        """Returns signature data from InRelease file

        Args:
            None

        Returns:
            Dict[str, Factinfo]
        """
        if 'signdata' in self.releasedata:
            return self.releasedata['signdata']

        return dict()

    @property
    def signaturehash(self) -> Factinfo:
        """Returns the hash of the signature

        Args:
            None

        Returns:
            Factinfo object of the signature hash
        """
        if 'signdata' in self.releasedata:
            if 'Hash' in self.releasedata['signdata']:
                return self.releasedata['signdata']['Hash']

        return Factinfo()

    @property
    def signatureversion(self) -> Factinfo:
        """Returns version of signature tool in signature

        Args:
            None

        Returns:
            Factinfo object of the signature version
        """
        if 'signdata' in self.releasedata:
            if 'Version' in self.releasedata['signdata']:
                return self.releasedata['signdata']['Version']

        return Factinfo()

    @property
    def signature(self) -> Factinfo:
        """Returns the signature

        Args:
            None

        Returns:
            Factinfo object of the signature
        """
        if 'signdata' in self.releasedata:
            if 'signature' in self.releasedata['signdata']:
                return self.releasedata['signdata']['signature']

        return Factinfo()

    @property
    def signed(self) -> bool:
        """Is the file signed?
        This function doesn't check if the signature is valid

        Args:
            None
        Returns:
            bool: Returns true if the file is signed with a signature
        """
        if 'signdata' in self.releasedata:
            if ('Version' in self.releasedata['signdata']) \
                and ('signature' in self.releasedata['signdata']) \
                    and ('Hash' in self.releasedata['signdata']):
                return True

        return False

    @property
    def releaseinfo(self) -> Dict[str, Any]:
        """Returns all header data from Release file"""
        if self.releasedata['headerdata']:
            return self.releasedata['headerdata']

        return dict()

    @property
    def origin(self) -> Factinfo:
        """Returns Origin: from Release file"""
        if 'Origin' in self.releasedata['headerdata']:
            return self.releasedata['headerdata']['Origin']

        return Factinfo()

    @origin.setter
    def origin(self, originvalue: str):
        if 'Origin' in self.releasedata['headerdata']:
            self.releasedata['headerdata']['Origin'].value = originvalue
        else:
            self.releasedata['headerdata']['Origin'] = Factinfo(
                'Origin', originvalue)

    @property
    def label(self) -> Factinfo:
        if 'Label' in self.releasedata['headerdata']:
            return self.releasedata['headerdata']['Label']

        return Factinfo()

    @label.setter
    def label(self, labelvalue):
        if 'Label' in self.releasedata['headerdata']:
            self.releasedata['headerdata']['Label'].value = labelvalue
        else:
            self.releasedata['headerdata']['Label'] = Factinfo(
                'Label', labelvalue)

    @property
    def suite(self) -> Factinfo:
        if 'Suite' in self.releasedata['headerdata']:
            return self.releasedata['headerdata']['Suite']

        return Factinfo()

    @suite.setter
    def suite(self, suitevalue):
        if 'Suite' in self.releasedata['headerdata']:
            self.releasedata['headerdata']['Suite'].value = suitevalue
        else:
            self.releasedata['headerdata']['Suite'] = Factinfo(
                'Suite', suitevalue)

    @property
    def version(self) -> Factinfo:
        if 'Version' in self.releasedata['headerdata']:
            return self.releasedata['headerdata']['Version']

        return Factinfo()

    @version.setter
    def version(self, versionvalue: str):
        if 'Version' in self.releasedata['headerdata']:
            self.releasedata['headerdata']['Version'].value = versionvalue
        else:
            self.releasedata['headerdata']['Version'] = Factinfo(
                'Version', versionvalue)

    @property
    def codename(self) -> Factinfo:
        if 'Codename' in self.releasedata['headerdata']:
            return self.releasedata['headerdata']['Codename']

        return Factinfo()

    @codename.setter
    def codename(self, codenamevalue: str):
        if 'Codename' in self.releasedata['headerdata']:
            self.releasedata['headerdata']['Codename'].value = codenamevalue
        else:
            self.releasedata['headerdata']['Codename'] = Factinfo(
                'Codename', codenamevalue)

    @property
    def date(self) -> Factinfo:
        if 'Date' in self.releasedata['headerdata']:
            return self.releasedata['headerdata']['Date']

        return Factinfo()

    @date.setter
    def date(self, datevalue):
        if 'Date' in self.releasedata['headerdata']:
            self.releasedata['headerdata']['Date'].value = datevalue
        else:
            self.releasedata['headerdata']['Date'] = Factinfo(
                'Date', datevalue)

    @property
    def validuntil(self) -> Factinfo:
        if 'Valid-Until' in self.releasedata['headerdata']:
            return self.releasedata['headerdata']['Valid-Until']

        return Factinfo()

    @validuntil.setter
    def validuntil(self, validuntilvalue: str):
        if 'Valid-Until' in self.releasedata['headerdata']:
            self.releasedata['headerdata']['Valid-Until'].value = validuntilvalue
        else:
            self.releasedata['headerdata']['Valid-Until'] = Factinfo(
                'Valid-Until', validuntilvalue)

    @property
    def components(self) -> Listinfo:
        """Returns a Listinfo object with the components"""
        if 'Components' in self.releasedata['headerdata']:
            return self.releasedata['headerdata']['Components']

        return Listinfo()

    @components.setter
    def components(self, componentsvalue: Union[str, List[str]]):
        """Adds the new component"""
        if 'Components' in self.releasedata['headerdata']:
            if isinstance(componentsvalue, str):
                self.releasedata['headerdata']['Components'].add(componentsvalue)
            elif isinstance(componentsvalue, list):
                for onecomponent in componentsvalue:
                    self.releasedata['headerdata']['Components'].add(onecomponent)
        else:
            if isinstance(componentsvalue, str):
                self.releasedata['headerdata']['Components'] = Listinfo(
                    'Components', componentsvalue)
            elif isinstance(componentsvalue, list):
                self.releasedata['headerdata']['Components'] = Listinfo(
                    'Components', '')
                for onecomponent in componentsvalue:
                    self.releasedata['headerdata']['Components'].add(onecomponent)

    @property
    def architectures(self) -> Listinfo:
        if 'Architectures' in self.releasedata['headerdata']:
            return self.releasedata['headerdata']['Architectures']

        return Listinfo()

    @architectures.setter
    def architectures(self, architecturesvalue: Union[str, list]):
        """Adds the new architecture"""
        if 'Architectures' in self.releasedata['headerdata']:
            if isinstance(architecturesvalue, str):
                self.releasedata['headerdata']['Architectures'].add(architecturesvalue)
            elif isinstance(architecturesvalue, list):
                for onearchitecture in architecturesvalue:
                    self.releasedata['headerdata']['Architectures'].add(onearchitecture)
        else:
            if isinstance(architecturesvalue, str):
                self.releasedata['headerdata']['Architectures'] = Listinfo(
                    'Architectures', architecturesvalue)
            elif isinstance(architecturesvalue, list):
                self.releasedata['headerdata']['Architectures'] = Listinfo(
                    'Architectures', '')
                for onearchitecture in architecturesvalue:
                    self.releasedata['headerdata']['Architectures'].add(onearchitecture)

    @property
    def description(self) -> Factinfo:
        if 'Description' in self.releasedata['headerdata']:
            return self.releasedata['headerdata']['Description']

        return Factinfo()

    @description.setter
    def description(self, descriptionvalue: str):
        if 'Description' in self.releasedata['headerdata']:
            self.releasedata['headerdata']['Description'].value = descriptionvalue
        else:
            self.releasedata['headerdata']['Description'] = Factinfo(
                'Description', descriptionvalue)

    @property
    def releasefiledata(self) -> Dict[str, Any]:
        return self.releasedata

    @property
    def notautomatic(self) -> Factinfo:
        if 'NotAutomatic' in self.releasedata['headerdata']:
            return self.releasedata['headerdata']['NotAutomatic']

        return Factinfo()

    @notautomatic.setter
    def notautomatic(self, notautomaticvalue: Union[str, bool]):
        newvalue = str()
        if isinstance(notautomaticvalue, bool):
            if notautomaticvalue:
                newvalue = 'yes'
            else:
                newvalue = 'no'
        else:
            newvalue = notautomaticvalue

        if 'NotAutomatic' in self.releasedata['headerdata']:
            self.releasedata['headerdata']['NotAutomatic'].value = newvalue
        else:
            self.releasedata['headerdata']['NotAutomatic'] = Factinfo(
                'NotAutomatic', newvalue)
        if newvalue == 'no':
            self.butnotautomaticupgrades = False

    @property
    def butautomaticupgrades(self) -> Factinfo:
        if 'ButAutomaticUpgrades' in self.releasedata['headerdata']:
            return self.releasedata['headerdata']['ButAutomaticUpgrades']

        return Factinfo()

    @butautomaticupgrades.setter
    def butautomaticupgrades(self, butautomaticupgradesvalue: Union[str, bool]):
        newvalue = str()
        if isinstance(butautomaticupgradesvalue, bool):
            if butautomaticupgradesvalue:
                newvalue = 'yes'
            else:
                newvalue = 'no'
        else:
            newvalue = butautomaticupgradesvalue

        if 'ButAutomaticUpgrades' in self.releasedata['headerdata']:
            self.releasedata['headerdata']['ButAutomaticUpgrades'].value = newvalue
        else:
            self.releasedata['headerdata']['ButAutomaticUpgrades'] = Factinfo(
                'ButAutomaticUpgrades', newvalue)
        if newvalue == 'yes':
            # The .setter is called and that is a boolean
            self.notautomatic = True  # type: ignore

    @property
    def acquirebyhash(self) -> Factinfo:
        if 'Acquire-By-Hash' in self.releasedata['headerdata']:
            return self.releasedata['headerdata']['Acquire-By-Hash']

        return Factinfo()

    @acquirebyhash.setter
    def acquirebyhash(self, acquirevalue: str):
        if 'Acquire-By-Hash' in self.releasedata['headerdata']:
            self.releasedata['headerdata']['Acquire-By-Hash'].value = acquirevalue
        else:
            self.releasedata['headerdata']['Acquire-By-Hash'] = Factinfo(
                'Acquire-By-Hash', acquirevalue)

    def repoformat(self) -> str:
        """If a slash is found in the files its a normal repo
           if no slash is found it's probably flatfile
        """
        # for onefile in self.files:
        #     fileinfo = self.file(onefile)
        #     if '/' in fileinfo.relativepath:
        #         return 'deb'
        if self.components.value == list():
            return 'flat'

        return 'deb'

    def signature_correct(self, public_key_file: str = '') -> bool:
        """Verify the InRelease file with the public key of the signer

        Args:
            public_key_file (str): Path to public key, for verification

        Returns:
            bool: True if the signature is correct

        """
        if not pgp_enabled:
            print('Module pgpy is not loaded!')
            return False
        key_file = str()
        if public_key_file != '':
            if self.signers_public_key_file != '':
                return False

        if public_key_file != '':
            key_file = public_key_file
        else:
            key_file = self.signers_public_key_file

        if not os.path.exists(key_file):
            return False

        public_signing_key = pgpy.PGPKey()

        with open(key_file, 'rt') as kf:
            public_signing_key.parse(kf.read())

        # print(f'Signed by {public_signing_key.fingerprint.keyid}')
        signed_message = pgpy.PGPMessage()
        signed_message.parse(self.rawdata)

        if public_signing_key.fingerprint.keyid not in signed_message.signers:
            return False

        # print(f'Signer in message is {signed_message.signers}')
        # signature_str = pgpy.PGPSignature()
        # signature_str.parse(self.rawdata)

        if public_signing_key.verify(signed_message):
            return True

        return False

    def sign(self, private_key_file: str = '') -> bool:
        """Sign the InRelease
        Sign the Release data to a InRelease file

        Args:
            private_key_file The path to the ascii armored private key file

        Returns:
            True if the signing was successfull
            False if the signing failed

        Raises:
            FileNotFoundError
        """

        private_key = pgpy.PGPKey()
        private_key, _ = pgpy.PGPKey.from_file(private_key_file)

        release_text = pgpy.PGPMessage.new(self.totext, cleartext=True)
        release_text |= private_key.sign(release_text)
        self.signature_data = str(release_text)

        return True

    def unknown(self, unknownkey: str) -> Factinfo:
        if unknownkey in self.releaseinfo:
            return self.releaseinfo[unknownkey]

        return Factinfo()

    @property
    def totext(self) -> str:
        if self.signature_data != '':
            return self.signature_data

        outstring = str()
        for onekey in self.releaseinfo.keys():
            outstring += self.releaseinfo[onekey].original + "\n"
        if self.files:
            if self.md5:
                outstring += "MD5Sum:\n"
            for file in self.files:
                outstring += self.fileinfo(file).totext('md5')
            if self.sha1:
                outstring += "SHA1:\n"
                for file in self.files:
                    outstring += self.fileinfo(file).totext('sha1')
            if self.sha384:
                outstring += "SHA384:\n"
                for file in self.files:
                    outstring += self.fileinfo(file).totext('sha384')
            if self.sha256:
                outstring += "SHA256:\n"
                for file in self.files:
                    outstring += self.fileinfo(file).totext('sha256')
            if self.sha512:
                outstring += "SHA512:\n"
                for file in self.files:
                    outstring += self.fileinfo(file).totext('sha512')

        return outstring.strip()
