# Handle SHA256SUMS files
# Copyright 2024 Thomas Karlsson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Thomas Karlsson thomas.karlsson relea.se

import os
from typing import Dict, List, Any


class Sha256sums:
    """Read Sha256sums file"""
    def __init__(self, url_base: str = str(), directory_base: str = str()):
        """Init

        Args:
            url_base (str): base_url for be able to present correct urls
                            E.g. http://se.archive.ubuntu.com/ubuntu/dists/focal/main/signed/linux-amd64/current/
            directory_base (str): base for the directory to be able to present correct directories

        Returns:
            object
        """
        self.rawdata: List[str] = list()
        self.processedshadata: Dict[str, Dict[str, Any]] = dict()
        self.url_base = url_base
        self.directory_base = directory_base
        self.filename = str()

    def load(self, shafile: str):
        """Load a SHA256SUMS file

        Args:
            shafile (str): File path to the SHA256SUMS file

        Returns:
            None
        """
        if not os.path.exists(shafile):
            raise FileNotFoundError(shafile)
        with open(shafile, mode='rt') as sumfile:
            self.rawdata = sumfile.readlines()

        self.filename = shafile
        self.shadata = self.__parsedata()

        self.version = '1.0'

    def url(self, filestring: str) -> str:
        return os.path.join(self.url_base.rstrip('/'), filestring)

    def path(self, filestring: str) -> str:
        return os.path.join(self.directory_base.rstrip('/'), filestring)

    def raw(self) -> List[str]:
        return self.rawdata

    def fileexists(self, filestring: str) -> bool:
        """If path exists in the SHA256SUMS file

        Args:
            filestring (str): The file path to search for

        Returns:
            True if is exists in the SHA256SUMS file
            False if it's missing in the SHA256SUMS file
        """
        if filestring in self.shadata.keys():
            return True

        return False

    def hashexists(self, hashstring: str) -> bool:
        """If the hash exists in the SHA256SUMS file

        Args:
            hashstring (str): The hash to search for

        Returns:
            True if hash exists in SHA256SUMS file
            False if hash is missing from SHA256SUMS file
        """
        for onefile in self.shadata.keys():
            if hashstring == self.shadata[onefile]['hash']:
                return True

        return False

    def type(self, filestring: str) -> str:
        """ Returns 'binary' or 'text' """
        if self.fileexists(filestring):
            return self.shadata[filestring]['type']

        return str()

    def filename2hash(self, filestring: str) -> str:
        """ Returns the hash for that file """
        if filestring in self.shadata.keys():
            return self.shadata[filestring]['hash']

        return str()

    def hash2filename(self, hashstring: str) -> str:
        """ Returns the filename with that hash """
        for onefile in self.shadata.keys():
            if hashstring == self.shadata[onefile]['hash']:
                return onefile

        return str()

    @property
    def filenames(self) -> List[str]:
        """ Returns a list with all filenames """
        return list(self.shadata.keys())

    @property
    def hashes(self) -> List[str]:
        """ Returns a list with all hashes """
        allhashes = list()
        for path in self.shadata.keys():
            allhashes.append(self.shadata[path]['hash'])

        return allhashes

    def __parsedata(self) -> Dict[str, Dict[str, Any]]:
        """ Returns a list with dictionaries of package data """
        allfiles = dict()
        # lineinfo = dict()
        for line in self.rawdata:
            lineinfo: Dict[str, str] = dict()
            data = line.strip().split(' ', maxsplit=1)
            lineinfo['hash'] = data[0]
            filename = str()
            if data[1][0] == '*':
                filename = data[1][1:].strip()
                lineinfo['type'] = 'binary'
            else:
                filename = data[1].lstrip()
                lineinfo['type'] = 'text'
            lineinfo['file'] = filename

            allfiles[filename] = lineinfo

        return allfiles

    @property
    def totext(self) -> str:
        outstring = str()
        for onefile in self.shadata.keys():
            outstring += self.shadata[onefile]['hash'] + ' '
            if self.shadata[onefile]['type'] == 'binary':
                outstring += '*'
            else:
                outstring += ' '
            outstring += self.shadata[onefile]['file']
            outstring += "\n"

        return outstring.rstrip()
