# Handle Commands files
# Copyright 2024 Thomas Karlsson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Thomas Karlsson thomas.karlsson relea.se

import os
import lzma
from typing import Dict, List, Any
from debianformat.debiantypes import Factinfo, Listinfo


class Commandinfo:
    """Create an object of a commmand
    name: acl
    version: 2.2.53-6
    commands: chacl,getfacl,setfacl
    """
    def __init__(self, commanddata: Dict[str, str]):
        """Parses each command

        Args:
            commanddata (Dict[str, str])
        """
        self.commandinfo: Dict[str, Any] = dict()
        for fact in commanddata.keys():
            if fact in ['commands']:
                self.commandinfo[fact] = Listinfo(fact, commanddata[fact], ',')
            else:
                self.commandinfo[fact] = Factinfo(fact, commanddata[fact])

    @property
    def commands(self) -> Listinfo:
        """Lists commands for this package

        Returns:
            Listinfo object of the commands
        """
        return self.commandinfo['commands']

    @property
    def version(self) -> Factinfo:
        """Version of the package

        Returns:
            Factinfo object of version
        """
        return self.commandinfo['version']

    @property
    def name(self) -> Factinfo:
        """Name of the package

        Returns:
            Factinfo object of package name
        """
        return self.commandinfo['name']

    @property
    def ignorecommands(self) -> Factinfo:
        """Ignore commands for this package

        Returns:
            Factinfo object of ignore commands
        """
        return self.commandinfo['ignore-commands']

    @property
    def visiblepkgname(self) -> Factinfo:
        """Visible package name

        Returns:
            Factinfo object of visible package name
        """
        return self.commandinfo['visible-pkgname']

    @property
    def totext(self) -> str:
        """Convert object to original text

        Returns:
            original text of package from Commands-[arch]
        """
        outstring = str()
        outstring += self.name.original + "\n"
        outstring += self.version.original + "\n"
        outstring += self.commands.original + "\n"
        if 'ignore-commands' in self.commandinfo:
            outstring += self.ignorecommands.original + "\n"
        if 'visible-pkgname' in self.commandinfo:
            outstring += self.visiblepkgname.original + "\n"

        return outstring.rstrip()


class headerinfo:
    """
    suite: focal
    component: main
    arch: amd64
    """
    def __init__(self, headerdata: Dict[str, str]):
        """Header info

        Args:
            dictionary (Dict[str, str])
        """
        self.headerdata = dict()
        for fact in headerdata.keys():
            self.headerdata[fact] = Factinfo(fact, headerdata[fact])

    @property
    def suite(self) -> Factinfo:
        """Suite for this Commands-.+.gz

        Returns:
            Factinfo object with containing e.g. focal
        """
        return self.headerdata['suite']

    @property
    def component(self) -> Factinfo:
        """Component for this Commands-.+.gz

        Returns:
            Factinfo object with containing e.g. main
        """
        return self.headerdata['component']

    @property
    def arch(self) -> Factinfo:
        """Architecture for this Commands-.+.gz

        Returns:
            Factinfo object with containing e.g. amd64
        """
        return self.headerdata['arch']

    @property
    def totext(self) -> str:
        """Convert object to original text

        Returns:
            original text of header from Commands-[arch]
        """
        outstring = str()
        outstring += self.suite.original + "\n"
        outstring += self.component.original + "\n"
        outstring += self.arch.original

        return outstring


class Commandsfile:
    """Reads the entire Commands file
    """
    def __init__(self):
        """Commands-.+.gz file
        """
        self.rawdata = list()

    def load(self, commandsfile: str):
        """Reads the Commands-.+.gz file

        Args:
            commandsfile (str): The location of the Commands-amd64.gz file
        """
        if not os.path.exists(commandsfile):
            raise FileNotFoundError(commandsfile)
        with lzma.open(commandsfile, mode='rt') as xzfile:
            self.rawdata = xzfile.readlines()

        self.commandsdata = self.__parsecommandsdata()
        self.processedcommandsdata = dict()
        for onepackage in self.commandsdata.keys():
            self.processedcommandsdata[onepackage] = Commandinfo(self.commandsdata[onepackage])
        self.headerdata = self.__parsecommandsdata(True)
        self.processedheaderdata = headerinfo(self.headerdata)

    def __parsecommandsdata(self, returnheader: bool = False) -> Dict[str, Any]:
        """Private function that Parses text

        Args:
            returnheader (bool): If True, then only return the header data
                                 If False, then return all commands, without header data

        Returns:
            Dict[str, Any]
        """
        packages = dict()
        commanddata: Dict[str, Any] = dict()
        headerdata = dict()
        foundcommands = False
        lastline = str()
        for line in self.rawdata:
            if line.strip() != '' and not foundcommands:
                headerdata[line.strip().split(':')[0].strip()] = line.strip().split(':')[1].strip()
            if line.strip() == '':
                if foundcommands:
                    packages[commanddata['name']] = commanddata
                    commanddata = dict()
                    continue
                if lastline == '':
                    foundcommands = True
                    if returnheader:
                        return headerdata
            if line.strip() != '' and foundcommands:
                data = line.strip().split(':', maxsplit=1)
                commanddata[data[0].strip()] = data[1].strip()

            lastline = line.strip()

        return packages

    @property
    def packages(self) -> List[str]:
        """Package names in Commands-.+.gz file

        Returns:
            List[str]: All package names in the file
        """
        return list(self.processedcommandsdata.keys())

    def package(self, packagename: str) -> Commandinfo:
        """One package in the file

        Args:
            packagename (str): Name of the package

        Returns:
            Commandinfo: A Commandinfo object for specified package
        """
        if packagename in self.commandsdata:
            return self.processedcommandsdata[packagename]

        return Commandinfo(dict())

    @property
    def header(self) -> headerinfo:
        """Header for the Commands file

        Returns:
            headerinfo: A headerinfo object with the header of the file
        """
        return self.processedheaderdata

    @property
    def totext(self) -> str:
        """Convert object to original text

        Returns:
            original text of the Commands-[arch] file
        """
        outstring = str()
        outstring += self.header.totext + "\n\n\n"
        for onepackage in self.packages:
            outstring += self.package(onepackage).totext + "\n\n"

        return outstring.rstrip() + "\n"
