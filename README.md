# Debianformat

Python class to handle debian repository files
Files handled are

* (In)Release
* Commands-(amd64|i386|other architectures)
* Packages(.gz|.xz|.lzma)
* SHA256SUMS
* Translations-(short name for country)
* Contents-(amd64|i386|other architectures)

## List all packages
```python

    from debianformat.debianformat import packagesfile

    focal_packages = packagesfile()
    focal_packages.load('Packages.gz')

    print(focal_packages.packages)
```

## List all files in Release file
```python

    from debianformat.debianformat import releasefile

    focal_release = releasefile()
    focal_release.load('Release')

    print(focal_release.files)
```

## Print architecture on main/binary-amd64/Packages.bz2
```python

    from debianformat.debianformat import releasefile

    focal_release = releasefile()
    focal_release.load('Release')

    packages_file = focal_release.file('main/binary-amd64/Packages.bz2')
    print(packages_file.architecture)
```
