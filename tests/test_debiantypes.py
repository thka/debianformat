from debianformat.debiantypes import (Factinfo, Listinfo,
                                      Packagelistinfo, Dependsinfo)


def test_factinfo():
    data = Factinfo('Package', 'test_package')
    assert data.name == 'Package'
    data.name = 'Date'
    assert data.name == 'Date'
    assert data.value == 'test_package'
    data.value = 'other_value'
    assert data.value == 'other_value'

    assert data.totext == 'other_value'
    assert data.original == 'Date: other_value'


def test_listinfo():
    data = Listinfo('List', 'firefox, test, another')
    assert data.name == 'List'
    data.name = 'Date'
    assert data.name == 'Date'
    assert data.value == ['firefox', 'test', 'another']
    data.add('added')
    assert data.value == ['firefox', 'test', 'another', 'added']
    data.remove('test')
    assert data.value == ['firefox', 'another', 'added']
    assert data.totext == 'firefox, another, added'
    assert data.original == 'Date: firefox, another, added'


def test_packagelistinfo():
    data = Packagelistinfo('Package-List', "firefox\nfirefox-locale-af\nfirefox-locale-an")
    assert data.name == 'Package-List'
    assert data.value == ['firefox', 'firefox-locale-af', 'firefox-locale-an']
    data.remove('firefox-locale-af')
    assert data.value == ['firefox', 'firefox-locale-an']
    assert data.totext == "firefox\nfirefox-locale-an"
    assert data.original == "Package-List: firefox\nfirefox-locale-an"


def test_dependsinfo():
    data = Dependsinfo('passwd, debconf (>= 0.5) | debconf-2.0')
    assert data.packages == ['passwd', 'debconf']
    assert data.dependencies == [[{'package': 'passwd', 'versionnumber': '', 'versioncompare': ''}],
                                 [{'package': 'debconf', 'versionnumber': '0.5', 'versioncompare': '>='},
                                  {'package': 'debconf-2.0', 'versionnumber': '', 'versioncompare': ''}]]
    assert data.add('apache2', '2.4', '>=') == [{'package': 'apache2', 'versionnumber': '2.4', 'versioncompare': '>='}]
    assert data.dependencies == [[{'package': 'passwd', 'versionnumber': '', 'versioncompare': ''}],
                                 [{'package': 'debconf', 'versionnumber': '0.5', 'versioncompare': '>='},
                                  {'package': 'debconf-2.0', 'versionnumber': '', 'versioncompare': ''}],
                                 [{'package': 'apache2', 'versionnumber': '2.4', 'versioncompare': '>='}]]
    # print(data)
    assert data.add(['testpackage', 'second'], ['3', '2'], ['>=', '<=']) == [{'package': 'testpackage', 'versionnumber': '3', 'versioncompare': '>='},
                                                                             {'package': 'second', 'versionnumber': '2', 'versioncompare': '<='}]
    assert data.dependencies == [[{'package': 'passwd', 'versionnumber': '', 'versioncompare': ''}],
                                 [{'package': 'debconf', 'versionnumber': '0.5', 'versioncompare': '>='},
                                  {'package': 'debconf-2.0', 'versionnumber': '', 'versioncompare': ''}],
                                 [{'package': 'apache2', 'versionnumber': '2.4', 'versioncompare': '>='}],
                                 [{'package': 'testpackage', 'versionnumber': '3', 'versioncompare': '>='},
                                 {'package': 'second', 'versionnumber': '2', 'versioncompare': '<='}]]
    assert data.remove('apache2', '2.4', '>=') == [[{'package': 'passwd', 'versionnumber': '', 'versioncompare': ''}],
                                                   [{'package': 'debconf', 'versionnumber': '0.5', 'versioncompare': '>='},
                                                    {'package': 'debconf-2.0', 'versionnumber': '', 'versioncompare': ''}],
                                                   [{'package': 'testpackage', 'versionnumber': '3', 'versioncompare': '>='},
                                                   {'package': 'second', 'versionnumber': '2', 'versioncompare': '<='}]]
    assert data.totext == 'passwd, debconf (>= 0.5) | debconf-2.0, testpackage (>= 3) | second (<= 2)'
