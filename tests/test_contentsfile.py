from debianformat.contentsfile import Contentsfile


def test_contents_parse():
    loadpackages = Contentsfile()
    loadpackages.load('package_files/Contents-amd64-brief.gz')
    assert loadpackages.fileinfo('var/yp/nicknames').info == [{'section': 'universe', 'category': 'net', 'package': 'nis'}, {'section': 'universe', 'category': 'net', 'package': 'yp-tools'}]
    loadpackages.fileinfo('var/yp/nicknames').add('extra', 'utils', 'krb5')
    assert loadpackages.fileinfo('var/yp/nicknames').info == [{'section': 'universe', 'category': 'net', 'package': 'nis'}, {'section': 'universe', 'category': 'net', 'package': 'yp-tools'}, {'section': 'krb5', 'category': 'extra', 'package': 'utils'}]
    loadpackages.fileinfo('var/yp/nicknames').remove('extra', 'utils', 'krb5')
    assert loadpackages.fileinfo('var/yp/nicknames').info == [{'section': 'universe', 'category': 'net', 'package': 'nis'}, {'section': 'universe', 'category': 'net', 'package': 'yp-tools'}]
    assert loadpackages.fileinfo('var/yp/nicknames').packages == ['nis', 'yp-tools']
    assert loadpackages.fileinfo('var/yp/nicknames').categories == ['net']
    assert loadpackages.fileinfo('var/yp/nicknames').sections == ['universe']
    assert loadpackages.fileinfo('var/yp/Makefile').packages == ['nis']
    assert loadpackages.fileinfo('var/spool/hylafax/config/zyxel-1496e-2.0').totext == 'var/spool/hylafax/config/zyxel-1496e-2.0		    universe/comm/hylafax-server'
    assert loadpackages.fileinfo('bin/rm').totext == 'bin/rm							    utils/coreutils'
    assert loadpackages.fileinfo('debian/libffmpeg-nvenc-dev/usr/include/ffnvcodec/dynlink_cuda.h').totext == 'debian/libffmpeg-nvenc-dev/usr/include/ffnvcodec/dynlink_cuda.h universe/devel/libffmpeg-nvenc-dev'
    assert loadpackages.fileinfo('usr/share/doc/valentina/examples/collection/pantsМ7.val.gz').totext == 'usr/share/doc/valentina/examples/collection/pantsМ7.val.gz universe/graphics/valentina'
