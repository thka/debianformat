import pytest
from debianformat.packagesfile import Packagesfile, Packageinfo

packagetest = """Package: zziplib-bin
Priority: optional
Section: utils
Installed-Size: 84
Maintainer: LIU Qi <liuqi82@gmail.com>
Architecture: amd64
Source: zziplib (0.13.56-1)
Version: 0.13.56-1+b1
Depends: libc6 (>= 2.3), libzzip-0-13 (>= 0.13.56), zlib1g (>= 1:1.1.4)
Filename: pool/main/z/zziplib/zziplib-bin_0.13.56-1+b1_amd64.deb
Size: 39982
MD5sum: 01d322e39a894c6d920d4382a1fb73c0
SHA1: f2c0e60eb2b0817b245a5961556e52bc1bcb1ac2
SHA256: 51f9b339608c88b997cbefea37195dc8aff3ae979dfe869a64848e8f4b603426
Description: library providing read access on ZIP-archives - binaries
 The zziplib library is intentionally lightweight, it offers the ability
 to easily extract data from files archived in a single zip file.
 Applications can bundle files into a single zip archive and access them.
 The implementation is based only on the (free) subset of compression
 with the zlib algorithm which is actually used by the zip/unzip tools.
 .
 This package contains some useful binaries to extract data from zip
 archives.
Homepage: http://zziplib.sourceforge.net"""

description = """Run a "live" preinstalled system from read-only media"""


gzpackages = Packagesfile()
gzpackages.load('package_files/Packages.normal.gz')

xzpackages = Packagesfile()
xzpackages.load('package_files/Packages.normal.xz')


def test_filenotfound_instantiate():
    with pytest.raises(FileNotFoundError):
        loadpackages = Packagesfile()
        loadpackages.load('file_not_found')


def test_xz_package_parse():
    assert xzpackages.package('zziplib-bin')[0].filename.totext == 'pool/main/z/zziplib/zziplib-bin_0.13.56-1+b1_amd64.deb'


def test_gz_package_parse():
    assert gzpackages.package('zziplib-bin')[0].filename.totext == 'pool/main/z/zziplib/zziplib-bin_0.13.56-1+b1_amd64.deb'


def test_packagesfile():
    packages = Packagesfile()
    packages.url_base = 'http://se.archive.ubuntu.com/ubuntu'
    packages.directory_base = '/misc/ubuntu'
    packages.load('package_files/Packages.normal')
    onepackage = packages.package('zziplib-bin')[0]

    # assert packages.url == 'http://se.archive.ubuntu.com/ubuntu/dists/focal/main/binary-amd64/Packages.normal'
    # assert packages.path == '/misc/ubuntu/dists/focal/main/binary-amd64/Packages.normal'
    assert onepackage.url == 'http://se.archive.ubuntu.com/ubuntu/pool/main/z/zziplib/zziplib-bin_0.13.56-1+b1_amd64.deb'
    assert onepackage.path == '/misc/ubuntu/pool/main/z/zziplib/zziplib-bin_0.13.56-1+b1_amd64.deb'
    assert isinstance(packages.package('zziplib-bin'), list) is True
    assert onepackage.totext == packagetest
    assert onepackage.priority.value == 'optional'
    assert onepackage.section.value == 'utils'
    assert onepackage.installed_size.value == '84'
    assert onepackage.maintainer.value == 'LIU Qi <liuqi82@gmail.com>'
    assert onepackage.architecture.value == 'amd64'
    assert onepackage.source.value == 'zziplib (0.13.56-1)'
    assert onepackage.version.value == '0.13.56-1+b1'
    assert onepackage.depends.dependencies == [[{'versioncompare': '>=', 'versionnumber': '2.3', 'package': 'libc6'}],
                                               [{'versioncompare': '>=', 'versionnumber': '0.13.56', 'package': 'libzzip-0-13'}],
                                               [{'versioncompare': '>=', 'versionnumber': '1:1.1.4', 'package': 'zlib1g'}]]
    assert onepackage.filename.value == 'pool/main/z/zziplib/zziplib-bin_0.13.56-1+b1_amd64.deb'
    assert onepackage.size.value == '39982'
    assert onepackage.md5.value == '01d322e39a894c6d920d4382a1fb73c0'
    assert onepackage.sha1.value == 'f2c0e60eb2b0817b245a5961556e52bc1bcb1ac2'
    assert onepackage.sha256.value == '51f9b339608c88b997cbefea37195dc8aff3ae979dfe869a64848e8f4b603426'
    assert onepackage.homepage.value == 'http://zziplib.sourceforge.net'


def test_duplicates():
    packages = Packagesfile()
    packages.load('package_files/Packages.duplicates')
    assert packages.package('avast')[0].version.value == '4.0.3-1'
    assert packages.package('avast')[1].version.value == '4.0.4-1~ubuntu20.04'
    assert len(packages.package('avast')) == 4

    assert packages.remove('avast', '5.x.3') is False
    assert packages.remove('avast', '4.0.4-1~ubuntu20.04') is True
    assert len(packages.package('avast')) == 3
    assert packages.remove('avast') is True
    assert packages.exists('avast') is False


def test_noenter():
    packages = Packagesfile()
    packages.load('package_files/Packages.noenter')
    onepackage = packages.package('zziplib-bin')[0]
    assert onepackage.homepage.value == 'http://zziplib.sourceforge.net'


def test_empty_file():
    packages = Packagesfile()
    packages.load('package_files/Packages.empty')
    assert packages.packages == []

    pack = Packagesfile()
    pack.load('package_files/Packages.garbage')
    assert packages.packages == []


def test_packagesfile_functions():
    packages = Packagesfile()
    packages.load('package_files/Packages.normal')
    # onepackage = packages.package('zziplib-bin')[0]
    assert packages.exists('zziplib-bin') is True
    assert packages.exists('non-existent') is False
    assert packages.packages == ['zziplib-bin', 'zzuf']
    assert packages.md5 == '37cce9f229d9d408f8a84cb5efe3ca0a'
    assert packages.sha1 == '060034ae9b51f4249f3646384342fff6decbd5b7'
    assert packages.sha256 == '439bee344ef6557c4adc5777a5ef82041a8feccdbed5a3b561ae9c046dc088ae'


def test_packagesfile_functions_two():
    packages = Packagesfile()
    packages.load('package_files/Packages.focal.gz')
    assert packages.fulldependencylist('aodh-common') == ['adduser', 'binutils', 'binutils-common',
                                                          'binutils-x86-64-linux-gnu', 'bzip2',
                                                          'debconf', 'debconf-2.0', 'dpkg',
                                                          'dpkg-dev', 'gcc-10-base', 'libaudit-common',
                                                          'libaudit1', 'libbinutils', 'libbz2-1.0',
                                                          'libc6', 'libcap-ng0', 'libcrypt1',
                                                          'libctf-nobfd0', 'libctf0', 'libdpkg-perl',
                                                          'libgcc-s1', 'liblzma5', 'libpam-modules',
                                                          'libpam0g', 'libpcre2-8-0', 'libselinux1',
                                                          'libsemanage-common', 'libsemanage1',
                                                          'libsepol1', 'libstdc++6', 'make',
                                                          'passwd', 'patch', 'perl:any', 'tar',
                                                          'xz-utils', 'zlib1g']
    assert packages.exists('aisleriot', version='1:3.22.9-1') is True
    assert packages.exists('aisleriot', version='x123') is False

    assert packages.package('casper')[0].tag.totext == 'admin::boot, admin::filesystem, implemented-in::shell, protocol::smb, role::plugin, scope::utility, special::completely-tagged, works-with-format::iso9660'
    assert packages.package('anacron')[0].depends.totext == 'debianutils (>= 1.7), lsb-base (>= 3.0-10), libc6 (>= 2.7)'


def test_sources_tuxedo():
    packages = Packagesfile()
    packages.load('package_files/Sources.tuxedo')
    onepackage = packages.package('firefox')[0]

    assert onepackage.version.value == '2:106.0.4~tux1'
    assert onepackage.binary.value == ['firefox', 'firefox-locale-af', 'firefox-locale-an', 'firefox-locale-ar', 'firefox-locale-ast', 'firefox-locale-az', 'firefox-locale-be', 'firefox-locale-bg', 'firefox-locale-bn', 'firefox-locale-br', 'firefox-locale-bs', 'firefox-locale-ca', 'firefox-locale-cak', 'firefox-locale-cs', 'firefox-locale-cy', 'firefox-locale-da', 'firefox-locale-de', 'firefox-locale-el', 'firefox-locale-en', 'firefox-locale-eo', 'firefox-locale-es', 'firefox-locale-et', 'firefox-locale-eu', 'firefox-locale-fa', 'firefox-locale-fi', 'firefox-locale-fr', 'firefox-locale-fy', 'firefox-locale-ga', 'firefox-locale-gd', 'firefox-locale-gl', 'firefox-locale-gn', 'firefox-locale-gu', 'firefox-locale-he', 'firefox-locale-hi', 'firefox-locale-hr', 'firefox-locale-hsb', 'firefox-locale-hu', 'firefox-locale-hy', 'firefox-locale-ia', 'firefox-locale-id', 'firefox-locale-is', 'firefox-locale-it', 'firefox-locale-ja', 'firefox-locale-ka', 'firefox-locale-kab', 'firefox-locale-kk', 'firefox-locale-km', 'firefox-locale-kn', 'firefox-locale-ko', 'firefox-locale-lt', 'firefox-locale-lv', 'firefox-locale-mk', 'firefox-locale-mr', 'firefox-locale-ms', 'firefox-locale-my', 'firefox-locale-nb', 'firefox-locale-ne', 'firefox-locale-nl', 'firefox-locale-nn', 'firefox-locale-oc', 'firefox-locale-pa', 'firefox-locale-pl', 'firefox-locale-pt', 'firefox-locale-ro', 'firefox-locale-ru', 'firefox-locale-si', 'firefox-locale-sk', 'firefox-locale-sl', 'firefox-locale-sq', 'firefox-locale-sr', 'firefox-locale-sv', 'firefox-locale-szl', 'firefox-locale-ta', 'firefox-locale-te', 'firefox-locale-th', 'firefox-locale-tr', 'firefox-locale-uk', 'firefox-locale-ur', 'firefox-locale-uz', 'firefox-locale-vi', 'firefox-locale-xh', 'firefox-locale-zh-hans', 'firefox-locale-zh-hant']
    assert onepackage.package_list.value == ['firefox deb web optional arch=amd64',
                                             'firefox-locale-af deb web optional arch=amd64',
                                             'firefox-locale-an deb web optional arch=amd64',
                                             'firefox-locale-ar deb web optional arch=amd64',
                                             'firefox-locale-ast deb web optional arch=amd64',
                                             'firefox-locale-az deb web optional arch=amd64',
                                             'firefox-locale-be deb web optional arch=amd64',
                                             'firefox-locale-bg deb web optional arch=amd64',
                                             'firefox-locale-bn deb web optional arch=amd64',
                                             'firefox-locale-br deb web optional arch=amd64',
                                             'firefox-locale-bs deb web optional arch=amd64',
                                             'firefox-locale-ca deb web optional arch=amd64',
                                             'firefox-locale-cak deb web optional arch=amd64',
                                             'firefox-locale-cs deb web optional arch=amd64',
                                             'firefox-locale-cy deb web optional arch=amd64',
                                             'firefox-locale-da deb web optional arch=amd64',
                                             'firefox-locale-de deb web optional arch=amd64',
                                             'firefox-locale-el deb web optional arch=amd64',
                                             'firefox-locale-en deb web optional arch=amd64',
                                             'firefox-locale-eo deb web optional arch=amd64',
                                             'firefox-locale-es deb web optional arch=amd64',
                                             'firefox-locale-et deb web optional arch=amd64',
                                             'firefox-locale-eu deb web optional arch=amd64',
                                             'firefox-locale-fa deb web optional arch=amd64',
                                             'firefox-locale-fi deb web optional arch=amd64',
                                             'firefox-locale-fr deb web optional arch=amd64',
                                             'firefox-locale-fy deb web optional arch=amd64',
                                             'firefox-locale-ga deb web optional arch=amd64',
                                             'firefox-locale-gd deb web optional arch=amd64',
                                             'firefox-locale-gl deb web optional arch=amd64',
                                             'firefox-locale-gn deb web optional arch=amd64',
                                             'firefox-locale-gu deb web optional arch=amd64',
                                             'firefox-locale-he deb web optional arch=amd64',
                                             'firefox-locale-hi deb web optional arch=amd64',
                                             'firefox-locale-hr deb web optional arch=amd64',
                                             'firefox-locale-hsb deb web optional arch=amd64',
                                             'firefox-locale-hu deb web optional arch=amd64',
                                             'firefox-locale-hy deb web optional arch=amd64',
                                             'firefox-locale-ia deb web optional arch=amd64',
                                             'firefox-locale-id deb web optional arch=amd64',
                                             'firefox-locale-is deb web optional arch=amd64',
                                             'firefox-locale-it deb web optional arch=amd64',
                                             'firefox-locale-ja deb web optional arch=amd64',
                                             'firefox-locale-ka deb web optional arch=amd64',
                                             'firefox-locale-kab deb web optional arch=amd64',
                                             'firefox-locale-kk deb web optional arch=amd64',
                                             'firefox-locale-km deb web optional arch=amd64',
                                             'firefox-locale-kn deb web optional arch=amd64',
                                             'firefox-locale-ko deb web optional arch=amd64',
                                             'firefox-locale-lt deb web optional arch=amd64',
                                             'firefox-locale-lv deb web optional arch=amd64',
                                             'firefox-locale-mk deb web optional arch=amd64',
                                             'firefox-locale-mr deb web optional arch=amd64',
                                             'firefox-locale-ms deb web optional arch=amd64',
                                             'firefox-locale-my deb web optional arch=amd64',
                                             'firefox-locale-nb deb web optional arch=amd64',
                                             'firefox-locale-ne deb web optional arch=amd64',
                                             'firefox-locale-nl deb web optional arch=amd64',
                                             'firefox-locale-nn deb web optional arch=amd64',
                                             'firefox-locale-oc deb web optional arch=amd64',
                                             'firefox-locale-pa deb web optional arch=amd64',
                                             'firefox-locale-pl deb web optional arch=amd64',
                                             'firefox-locale-pt deb web optional arch=amd64',
                                             'firefox-locale-ro deb web optional arch=amd64',
                                             'firefox-locale-ru deb web optional arch=amd64',
                                             'firefox-locale-si deb web optional arch=amd64',
                                             'firefox-locale-sk deb web optional arch=amd64',
                                             'firefox-locale-sl deb web optional arch=amd64',
                                             'firefox-locale-sq deb web optional arch=amd64',
                                             'firefox-locale-sr deb web optional arch=amd64',
                                             'firefox-locale-sv deb web optional arch=amd64',
                                             'firefox-locale-szl deb web optional arch=amd64',
                                             'firefox-locale-ta deb web optional arch=amd64',
                                             'firefox-locale-te deb web optional arch=amd64',
                                             'firefox-locale-th deb web optional arch=amd64',
                                             'firefox-locale-tr deb web optional arch=amd64',
                                             'firefox-locale-uk deb web optional arch=amd64',
                                             'firefox-locale-ur deb web optional arch=amd64',
                                             'firefox-locale-uz deb web optional arch=amd64',
                                             'firefox-locale-vi deb web optional arch=amd64',
                                             'firefox-locale-xh deb web optional arch=amd64',
                                             'firefox-locale-zh-hans deb web optional arch=amd64',
                                             'firefox-locale-zh-hant deb web optional arch=amd64']
    source_files = onepackage.files
    assert source_files == [{'md5': '094889265378f28b7c40af035b75fed6',
                             'size': 6828,
                             'filename': 'firefox_106.0.4~tux1.dsc',
                             'sha1': '3da85e88ba5174b139e249f0184f1483726cead6',
                             'sha256': 'f5547f5e9c6ebcbe3b0d7b4a4e77984f5ca27ff346a7c1e125506fd5e04211a8'},
                            {'md5': '11c5ecd32816a1fb4a8baee508cf221f',
                             'size': 118660568,
                             'filename': 'firefox_106.0.4~tux1.tar.xz',
                             'sha1': '9b7287c09650017e415cb4dff90819b4b24105ab',
                             'sha256': '5945c91d8d7d44cddda1eaed9be6131477785edf78b89663d4b8619f2fb85cd2'}]
    assert onepackage.checksums_sha1.value == ['3da85e88ba5174b139e249f0184f1483726cead6 6828 firefox_106.0.4~tux1.dsc',
                                               '9b7287c09650017e415cb4dff90819b4b24105ab 118660568 firefox_106.0.4~tux1.tar.xz']
    assert onepackage.checksums_sha256.value == ['f5547f5e9c6ebcbe3b0d7b4a4e77984f5ca27ff346a7c1e125506fd5e04211a8 6828 firefox_106.0.4~tux1.dsc',
                                                 '5945c91d8d7d44cddda1eaed9be6131477785edf78b89663d4b8619f2fb85cd2 118660568 firefox_106.0.4~tux1.tar.xz']


def test_translation_files():
    packages = Packagesfile()
    packages.load('package_files/Translation-sv.gz')
    # assert packages.language == 'sv'
    assert packages.package('libreadline8')[0].languages == ['sv']
    assert packages.package('libsane')[0].description_md5.value == '2f46c9804313fd609f2868bde7d36408'
    assert packages.package('libsane')[0].description_intl().value == """API library for scanners
 SANE står för "Scanner Access Now Easy" (skanneråtkomst är nu enkel) och
 är ett programmeringsgränssnitt (API) som erbjuder standardiserad åtkomst
 till all skannerhårdvara baserad på rasterbilder (flatbädsskanner,
 handhållen skanner, bildfångare, video- och stillbildskameror, etc.).
 SANE-standarden är fri och vem som helst får delta i dess utveckling och
 diskussioner. Den nuvarande källkoden är skriven för att stöda flera
 operativsystem, inklusive GNU/Linux, OS/2, Win32 och diverse Unix-
 varianter, och finns tillgänglig under GNU General Public License (även
 kommersiella tillämpningar och hårdvarugränssnitt är välkomna).
 .
 Graphical frontends for sane are available in the packages sane and xsane.
 Command line frontend scanimage, saned and sane-find-scanner are available
 in the sane-utils package."""
