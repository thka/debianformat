# import pytest
from debianformat.sha256sums import Sha256sums


original = """901e581e18fb9a1cba035ae36f8c7047ea96294ee024fa614d1a87f01b86e98f *fwupdx64.efi
13e4f5fee6f524e8034eda66207325e73a6a57b764a8e79d46f56ca056de9518 *fwupdx64.efi.signed
3de885ec1b632cf14e2491922de76a42770579b603557119ade373ed201bcd97  version
25499f8bd39f3e79f867ff26a316436f6e757514b04d300c18fde220f3d34479 *control/uefi.crt"""


def test_sha256sums():
    shasums = Sha256sums('http://mirror.accum.se/ubuntu/dists/focal/main/uefi/grub2-amd64/current',
                         '/misc/ubuntu/dists/focal/main/uefi/grub2-amd64/current')
    shasums.load('package_files/SHA256SUMS')
    assert shasums.filenames == ['fwupdx64.efi',
                                 'fwupdx64.efi.signed',
                                 'version',
                                 'control/uefi.crt']
    assert shasums.fileexists('fwupdx64.efi') is True
    assert shasums.fileexists('fwupdx64.efi.test') is False
    assert shasums.hashexists('13e4f5fee6f524e8034eda66207325e73a6a57b764a8e79d46f56ca056de9518') is True
    assert shasums.hashexists('aaaaaaaaaaaaaaaaaaaaaaaaaa7325e73a6a57b764a8e79d46f56ca056de9518') is False
    assert shasums.hashes == ['901e581e18fb9a1cba035ae36f8c7047ea96294ee024fa614d1a87f01b86e98f',
                              '13e4f5fee6f524e8034eda66207325e73a6a57b764a8e79d46f56ca056de9518',
                              '3de885ec1b632cf14e2491922de76a42770579b603557119ade373ed201bcd97',
                              '25499f8bd39f3e79f867ff26a316436f6e757514b04d300c18fde220f3d34479']
    assert shasums.type('fwupdx64.efi.signed') == 'binary'
    assert shasums.type('version') == 'text'
    assert shasums.type('non-existent') == ''
    assert shasums.filename2hash('fwupdx64.efi.signed') == '13e4f5fee6f524e8034eda66207325e73a6a57b764a8e79d46f56ca056de9518'
    assert shasums.hash2filename('901e581e18fb9a1cba035ae36f8c7047ea96294ee024fa614d1a87f01b86e98f') == 'fwupdx64.efi'
    assert shasums.totext == original
    assert shasums.url('fwupdx64.efi.signed') == 'http://mirror.accum.se/ubuntu/dists/focal/main/uefi/grub2-amd64/current/fwupdx64.efi.signed'
    assert shasums.path('fwupdx64.efi.signed') == '/misc/ubuntu/dists/focal/main/uefi/grub2-amd64/current/fwupdx64.efi.signed'
