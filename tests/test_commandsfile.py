from debianformat.commandsfile import Commandsfile


def test_commands_parse():
    loadpackages = Commandsfile()
    loadpackages.load('package_files/Commands-amd64.xz')
    assert loadpackages.package('alsa-utils').version.value == '1.2.2-1ubuntu1'
    assert loadpackages.package('alsa-utils').commands.totext == 'aconnect,alsa-info,alsabat,alsabat-test,alsactl,alsaloop,alsamixer,alsatplg,alsaucm,amidi,amixer,aplay,aplaymidi,arecord,arecordmidi,aseqdump,aseqnet,axfer,iecset,speaker-test'
    assert loadpackages.package('alsa-utils').name.totext == 'alsa-utils'
    loadpackages.package('alsa-utils').commands.add('extra_command')
    assert loadpackages.package('alsa-utils').commands.totext == 'aconnect,alsa-info,alsabat,alsabat-test,alsactl,alsaloop,alsamixer,alsatplg,alsaucm,amidi,amixer,aplay,aplaymidi,arecord,arecordmidi,aseqdump,aseqnet,axfer,iecset,speaker-test,extra_command'
    loadpackages.package('alsa-utils').commands.remove('alsa-info')
    assert loadpackages.package('alsa-utils').commands.totext == 'aconnect,alsabat,alsabat-test,alsactl,alsaloop,alsamixer,alsatplg,alsaucm,amidi,amixer,aplay,aplaymidi,arecord,arecordmidi,aseqdump,aseqnet,axfer,iecset,speaker-test,extra_command'
    assert loadpackages.package('python-is-python3').ignorecommands.value == 'python'
    assert loadpackages.package('python3-minimal').visiblepkgname.value == 'python3'

    assert loadpackages.header.suite.value == 'focal'
    loadpackages.header.suite.value = 'xenial'
    assert loadpackages.header.suite.value == 'xenial'
    assert loadpackages.header.component.value == 'main'
    assert loadpackages.header.arch.value == 'amd64'
