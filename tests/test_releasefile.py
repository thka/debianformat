# type: ignore
from debianformat.releasefile import Releasefile, Releasefileinfo


def test_release_parse():
    loadpackages = Releasefile()
    assert loadpackages.url == 'dists/'
    assert loadpackages.path == 'dists/'
    loadpackages.url_base = 'http://se.archive.ubuntu.com/ubuntu/dists/focal'
    loadpackages.directory_base = '/misc/ubuntu/dists/focal'
    loadpackages.load('package_files/Release.focal')


    loadpackages.url_base = 'https://se.archive.ubuntu.com/ubuntu/'
    loadpackages.directory_base = '/misc/ubuntu/'
    assert loadpackages.url == 'https://se.archive.ubuntu.com/ubuntu/dists/focal/Release.focal'
    assert loadpackages.path == '/misc/ubuntu/dists/focal/Release.focal'

    loadpackages.url_base = 'https://se.archive.ubuntu.com/ubuntu'
    loadpackages.directory_base = '/misc/ubuntu'
    assert loadpackages.url == 'https://se.archive.ubuntu.com/ubuntu/dists/focal/Release.focal'
    assert loadpackages.path == '/misc/ubuntu/dists/focal/Release.focal'

    assert loadpackages.exists('Contents-arm64') is True
    assert loadpackages.exists('random_file') is False
    loadpackages.fileinfo('Contents-arm64').digest('11111ed0539db355d1aab3c54717d79d')
    assert loadpackages.fileinfo('Contents-arm64').url == 'http://se.archive.ubuntu.com/ubuntu/dists/focal/Contents-arm64'
    assert loadpackages.fileinfo('Contents-arm64').md5 == '11111ed0539db355d1aab3c54717d79d'
    assert loadpackages.fileinfo('Contents-arm64').sha1 != '11111ed0539db355d1aab3c54717d79d'
    assert loadpackages.components.value == ['main', 'restricted', 'universe', 'multiverse']
    loadpackages.components.add('test')
    assert loadpackages.components.value == ['main', 'restricted', 'universe', 'multiverse', 'test']
    loadpackages.components.remove('test')
    assert loadpackages.components.value == ['main', 'restricted', 'universe', 'multiverse']
    assert loadpackages.components.totext == 'main restricted universe multiverse'
    assert loadpackages.version.totext == '20.04'
    assert loadpackages.version.original == 'Version: 20.04'
    loadpackages.version = '18.04'
    loadpackages.version.value = '18.04'
    assert loadpackages.version.totext == '18.04'
    assert loadpackages.version.original == 'Version: 18.04'
    assert len(loadpackages.packages_files('nonexist', 'amd64')) == 0
    assert len(loadpackages.packages_files('main', 'nonexist')) == 0
    assert len(loadpackages.packages_files('main', 'amd64')) == 3
    assert len(loadpackages.packages_files('main')) == 21
    assert len(loadpackages.packages_files()) == 168

    jenkins = Releasefile()
    jenkins.load('package_files/Release.jenkins')
    assert len(jenkins.packages_files()) == 4
    # assert len(jenkins.packages_files(architecture='all')) == 4
    assert jenkins.components.value == []
    assert jenkins.architectures.value == ['all']

    diffrelease = Releasefile()
    diffrelease.load('package_files/Release.diff')
    assert len(diffrelease.packages_files('main', 'amd64')) == 3
    assert len(diffrelease.packages_files('main', 'amd64', True)) == 1


def test_inrelease(inrelease_file_elastic):
    inrelease = Releasefile()
    inrelease.load(inrelease_file_elastic)
    assert inrelease.signature_correct('package_files/GPG-KEY-elasticsearch') is True
    # assert inrelease.signatureversion.value == 'GnuPG v1'
    assert inrelease.signaturehash.value == 'SHA256'

    inrelease.load('package_files/InRelease.elastic.badsignature')
    assert inrelease.signature_correct('GPG-KEY-elasticsearch') is False


def test_createown(release_file_focal):
    myrelease = Releasefile()
    # myrelease.load(release_file_focal)
    assert myrelease.origin.value == ''
    myrelease.origin = 'Ubuntu'
    assert myrelease.origin.value == 'Ubuntu'
    assert myrelease.codename.value == ''
    myrelease.codename = 'focal'
    assert myrelease.codename.value == 'focal'
    myrelease.components = 'main'
    assert myrelease.components.value == ['main']
    myrelease.components = ['restricted', 'universe']
    assert myrelease.components.value == ['main', 'restricted', 'universe']
    myrelease.date = '2021-06-14'
    assert myrelease.date.value == '2021-06-14'
    myrelease.architectures = 'amd64'
    assert myrelease.architectures.value == ['amd64']
    myrelease.architectures = ['i386', 'powerpc']
    assert myrelease.architectures.value == ['amd64', 'i386', 'powerpc']
    myrelease.description = 'This is the description'
    assert myrelease.description.value == 'This is the description'
    myrelease.acquirebyhash = 'yes'
    assert myrelease.acquirebyhash.value == 'yes'
    assert myrelease.notautomatic.value == ''
    myrelease.butautomaticupgrades = True
    assert myrelease.notautomatic.value == 'yes'
    assert myrelease.butautomaticupgrades.value == 'yes'


def test_fileinfo_funcs():
    loadpackages = Releasefile()
    loadpackages.load('package_files/Release.focal')
    filedata = loadpackages.fileinfo('main/binary-amd64/Packages.xz')
    assert filedata.md5 == '9979260eea32514325bcd05e9e651368'
    assert filedata.relativedirectory == 'main/binary-amd64'
    assert filedata.relativepath == 'main/binary-amd64/Packages.xz'
    assert filedata.filename == 'Packages.xz'
    assert filedata.language == 'unknown'
    assert filedata.architecture == 'amd64'
    assert filedata.function == 'binary-amd64'
    assert filedata.component == 'main'
    assert filedata.filename_byhash == '7757921ff8feed9c3934a0c9936d441ba4a238bee3ea6c8c1df5cbcd43fc9861'
    assert filedata.relativepath_byhash == 'main/binary-amd64/by-hash/SHA256/7757921ff8feed9c3934a0c9936d441ba4a238bee3ea6c8c1df5cbcd43fc9861'
